#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>

struct DVHelper {
    static int* f_int(void *p, int offset) { return reinterpret_cast<int*>(reinterpret_cast<char*>(p)+offset); }
    static long* f_long(void *p, int offset) { return reinterpret_cast<long*>(reinterpret_cast<char*>(p)+offset); }
    static char* f_char(void *p, int offset) { return reinterpret_cast<char*>(reinterpret_cast<char*>(p)+offset); }
    static void split(const std::string &s, char delim, std::vector<std::string> &elems) {
         std::stringstream ss(s);
         std::string item;
         while (std::getline(ss, item, delim)) {
             elems.push_back(item);
         }
     }
};

struct AAA 
{
    void * base__;
    AAA(void *b) : base__(b) {}
    AAA(void* p, const AAA& rh) : base__(p) {
	    a_() = rh.a();
	    c_() = rh.c();
	    b_() = rh.b();
	    d_() = rh.d();
    }
    static const char * dict() { return "!AAA|1|0|4,a|int|4|0,c|char|1|4,b|long|8|8,d|char|1|16"; }
	uint16_t size__ = 20;
    size_t size() { return size__; }
	void* base() { return base__; }
	template<typename T>
	T* base() { return reinterpret_cast<T*>(base__); }
    void rebase(void *p) { base__ = p; }
    uint16_t a_offset__=0;
    int a() const { return *DVHelper::f_int(base__,a_offset__); }
    int& a_() { return *DVHelper::f_int(base__,a_offset__); }
    uint16_t c_offset__=4;
    char c() const { return *DVHelper::f_char(base__,c_offset__); }
    char& c_() { return *DVHelper::f_char(base__,c_offset__); }
    uint16_t b_offset__=8;
    long b() const { return *DVHelper::f_long(base__,b_offset__); }
    long& b_() { return *DVHelper::f_long(base__,b_offset__); }
    uint16_t d_offset__=16;
    char d() const { return *DVHelper::f_char(base__,d_offset__); }
    char& d_() { return *DVHelper::f_char(base__,d_offset__); }
    void project() {
        a_offset__ = 0;
        c_offset__ = 4;
        b_offset__ = 8;
        d_offset__ = 16;
	    size__ = 20;
    }
    bool project(const char * d) { 
		uint16_t size = 0;
		uint16_t aligned = 1;
        std::vector<std::string> dic_table; 
        DVHelper::split(d, ',', dic_table); 
        for(auto e : dic_table) { 
            std::vector<std::string> items; 
            DVHelper::split(e, '|', items); 
			if(e[0] == '!') {
				if(items[0] != "!AAA")
				    return false;
				aligned = std::atoi(items[3].c_str());
				continue;
			}
            if(items[0] == "a" && items[1] == "int") { 
                a_offset__ = std::atoi(items[3].c_str()); 
				if(size <= a_offset__)
					size = a_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "c" && items[1] == "char") { 
                c_offset__ = std::atoi(items[3].c_str()); 
				if(size <= c_offset__)
					size = c_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "b" && items[1] == "long") { 
                b_offset__ = std::atoi(items[3].c_str()); 
				if(size <= b_offset__)
					size = b_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "d" && items[1] == "char") { 
                d_offset__ = std::atoi(items[3].c_str()); 
				if(size <= d_offset__)
					size = d_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
        }
		size__ = ((size+aligned-1)/aligned)*aligned;
		return true;
    }
};

struct AAAA 
{
    void * base__;
    AAAA(void *b) : base__(b) {}
    AAAA(void* p, const AAAA& rh) : base__(p) {
	    a_() = rh.a();
	    b_() = rh.b();
	    d_() = rh.d();
	    e_() = rh.e();
	    c_() = rh.c();
    }
    static const char * dict() { return "!AAAA|1|0|4,a|int|4|0,b|long|8|4,d|char|1|12,e|char|1|13,c|char|1|14"; }
	uint16_t size__ = 16;
    size_t size() { return size__; }
	void* base() { return base__; }
	template<typename T>
	T* base() { return reinterpret_cast<T*>(base__); }
    void rebase(void *p) { base__ = p; }
    uint16_t a_offset__=0;
    int a() const { return *DVHelper::f_int(base__,a_offset__); }
    int& a_() { return *DVHelper::f_int(base__,a_offset__); }
    uint16_t b_offset__=4;
    long b() const { return *DVHelper::f_long(base__,b_offset__); }
    long& b_() { return *DVHelper::f_long(base__,b_offset__); }
    uint16_t d_offset__=12;
    char d() const { return *DVHelper::f_char(base__,d_offset__); }
    char& d_() { return *DVHelper::f_char(base__,d_offset__); }
    uint16_t e_offset__=13;
    char e() const { return *DVHelper::f_char(base__,e_offset__); }
    char& e_() { return *DVHelper::f_char(base__,e_offset__); }
    uint16_t c_offset__=14;
    char c() const { return *DVHelper::f_char(base__,c_offset__); }
    char& c_() { return *DVHelper::f_char(base__,c_offset__); }
    void project() {
        a_offset__ = 0;
        b_offset__ = 4;
        d_offset__ = 12;
        e_offset__ = 13;
        c_offset__ = 14;
	    size__ = 16;
    }
    bool project(const char * d) { 
		uint16_t size = 0;
		uint16_t aligned = 1;
        std::vector<std::string> dic_table; 
        DVHelper::split(d, ',', dic_table); 
        for(auto e : dic_table) { 
            std::vector<std::string> items; 
            DVHelper::split(e, '|', items); 
			if(e[0] == '!') {
				if(items[0] != "!AAAA")
				    return false;
				aligned = std::atoi(items[3].c_str());
				continue;
			}
            if(items[0] == "a" && items[1] == "int") { 
                a_offset__ = std::atoi(items[3].c_str()); 
				if(size <= a_offset__)
					size = a_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "b" && items[1] == "long") { 
                b_offset__ = std::atoi(items[3].c_str()); 
				if(size <= b_offset__)
					size = b_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "d" && items[1] == "char") { 
                d_offset__ = std::atoi(items[3].c_str()); 
				if(size <= d_offset__)
					size = d_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "e" && items[1] == "char") { 
                e_offset__ = std::atoi(items[3].c_str()); 
				if(size <= e_offset__)
					size = e_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "c" && items[1] == "char") { 
                c_offset__ = std::atoi(items[3].c_str()); 
				if(size <= c_offset__)
					size = c_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
        }
		size__ = ((size+aligned-1)/aligned)*aligned;
		return true;
    }
};

struct BBB : public AAA
{
    BBB(void *b) : AAA(b) {}
    BBB(void* p, const BBB& rh) : AAA(p, rh) {
	    a_() = rh.a();
	    b_() = rh.b();
	    std::memmove(c_(), rh.c(), 40);
    }
    static const char * dict() { return "!BBB:AAA|1|0|4,a|int|4|20,b|int|4|24,c|int|40|28"; }
	uint16_t size__ = 68;
    size_t size() { return size__; }
    void* base() { return AAA::base(); }
	template <typename T>
	T* base() { return reinterpret_cast<T*>(AAA::base()); }
	void rebase(void *p) { AAA::rebase(p); }
    uint16_t a_offset__=20;
    int a() const { return *DVHelper::f_int(base__,a_offset__); }
    int& a_() { return *DVHelper::f_int(base__,a_offset__); }
    uint16_t b_offset__=24;
    int b() const { return *DVHelper::f_int(base__,b_offset__); }
    int& b_() { return *DVHelper::f_int(base__,b_offset__); }
    uint16_t c_offset__=28;
    const int* c() const { return DVHelper::f_int(base__,c_offset__); }
    int* c_() { return DVHelper::f_int(base__,c_offset__); }
    void project() {
        a_offset__ = 20;
        b_offset__ = 24;
        c_offset__ = 28;
	    size__ = 68;
    }
    bool project(const char * d) { 
		uint16_t size = 0;
		uint16_t aligned = 1;
        std::vector<std::string> dic_table; 
        DVHelper::split(d, ',', dic_table); 
        for(auto e : dic_table) { 
            std::vector<std::string> items; 
            DVHelper::split(e, '|', items); 
			if(e[0] == '!') {
				if(items[0] != "!BBB:AAA")
				    return false;
				aligned = std::atoi(items[3].c_str());
				continue;
			}
            if(items[0] == "a" && items[1] == "int") { 
                a_offset__ = std::atoi(items[3].c_str()); 
				if(size <= a_offset__)
					size = a_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "b" && items[1] == "int") { 
                b_offset__ = std::atoi(items[3].c_str()); 
				if(size <= b_offset__)
					size = b_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "c" && items[1] == "int") { 
                c_offset__ = std::atoi(items[3].c_str()); 
				if(size <= c_offset__)
					size = c_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
        }
		size__ = ((size+aligned-1)/aligned)*aligned;
		return true;
    }
};

struct CCC 
{
    void * base__;
    CCC(void *b) : base__(b),__a__(reinterpret_cast<char*>(base__)+a_offset__) {}
    CCC(void* p, const CCC& rh) : base__(p),__a__(base<char>()+a_offset__) {
	    a_() = rh.a();
	    b_() = rh.b();
	    c_() = rh.c();
	    cc_() = rh.cc();
    }
    static const char * dict() { return "!CCC|1|0|4,a|BBB|68|0,b|int|4|68,c|char|1|72,cc|char|1|73"; }
	uint16_t size__ = 76;
    size_t size() { return size__; }
	void* base() { return base__; }
	template<typename T>
	T* base() { return reinterpret_cast<T*>(base__); }
    void rebase(void *p) { base__ = p; }
    uint16_t a_offset__=0;
    BBB __a__;
    const BBB & a() const { return __a__; }
    BBB& a_() { return __a__; }
    uint16_t b_offset__=68;
    int b() const { return *DVHelper::f_int(base__,b_offset__); }
    int& b_() { return *DVHelper::f_int(base__,b_offset__); }
    uint16_t c_offset__=72;
    char c() const { return *DVHelper::f_char(base__,c_offset__); }
    char& c_() { return *DVHelper::f_char(base__,c_offset__); }
    uint16_t cc_offset__=73;
    char cc() const { return *DVHelper::f_char(base__,cc_offset__); }
    char& cc_() { return *DVHelper::f_char(base__,cc_offset__); }
    void project() {
        a_offset__ = 0;
        b_offset__ = 68;
        c_offset__ = 72;
        cc_offset__ = 73;
	    size__ = 76;
    }
    bool project(const char * d) { 
		uint16_t size = 0;
		uint16_t aligned = 1;
        std::vector<std::string> dic_table; 
        DVHelper::split(d, ',', dic_table); 
        for(auto e : dic_table) { 
            std::vector<std::string> items; 
            DVHelper::split(e, '|', items); 
			if(e[0] == '!') {
				if(items[0] != "!CCC")
				    return false;
				aligned = std::atoi(items[3].c_str());
				continue;
			}
            if(items[0] == "a" && items[1] == "BBB") { 
                a_offset__ = std::atoi(items[3].c_str()); 
				if(size <= a_offset__)
					size = a_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "b" && items[1] == "int") { 
                b_offset__ = std::atoi(items[3].c_str()); 
				if(size <= b_offset__)
					size = b_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "c" && items[1] == "char") { 
                c_offset__ = std::atoi(items[3].c_str()); 
				if(size <= c_offset__)
					size = c_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
            if(items[0] == "cc" && items[1] == "char") { 
                cc_offset__ = std::atoi(items[3].c_str()); 
				if(size <= cc_offset__)
					size = cc_offset__ + std::atoi(items[2].c_str());
				continue;
            } 
        }
		size__ = ((size+aligned-1)/aligned)*aligned;
		return true;
    }
};

