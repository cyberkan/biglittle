%{
    #include "dataview.tab.hh"

%}

%option noyywrap

%%

"%version"              { return DIR_VERSION; }
"%aligned"              { return DIR_ALIGNED; }
"struct"                { return STRUCT; }
"{"                     { return OBLOCK; }
"}"                     { return CBLOCK; }
"char"|"int8_t"         { return CHAR; }
"short"|"int16_t"       { return SHORT; }
"int"|"int32_t"         { return INT; }
"long"|"int64_t"        { return LONG; }
";"                     { return SEMICOLON; }
":"                     { return COLON; }
"["                     { return OB; }
"]"                     { return CB; }
[0-9]+                  { yylval.val = atoi(yytext); return NUMBER; }
[a-zA-Z_]               { yylval.cstr = strdup(yytext); return IDENTIFIER; }
[a-zA-Z_][a-zA-Z0-9_]*[a-zA-Z0-9]   { yylval.cstr = strdup(yytext); return IDENTIFIER; }
\n                      /* ignore */
[ \t]+                  /* ignore */

%%