#include <iostream>
#include <string>
#include <chrono>
#include <cstdio>
#include "sample.h"

struct A
{
    int a;
    long b;
	char c;
};

struct B {
	A a;
	long b;
	int c[10];
};

struct C {
	B a;
	int b;
	char c;
	char cc;

};
void print_hex(const char *str, const char * buf, size_t n)
{
	printf("%s : \n", str);
	for(int i = 0; i < n; ++i) {
		printf("%02x", buf[i]);
		if(!((i+1) % 4)) printf(" ");
		if(!((i+1) % 16)) printf("\n");
	}
}

int main()

{
	char buf[1024] = {};
{
	AAA a(buf);
	AAAA aa(buf);
	aa.a_() = 3;
	aa.b_() = 4;
	aa.c_() = 5;
	std::cout << "AAAA=" << aa.size() << ", aa.a=" << aa.a() << 
				 " aa.b=" << aa.b() << " aa.c=" << (int)aa.c() <<std::endl;
	std::cout << "AAA="<< a.size() << ", a.a=" << a.a() << 
				 " a.b=" << a.b() << " a.c=" << (int)a.c() <<std::endl;
	const char * dict = "!AAA|1|0|4,a|int|4|0,b|long|8|4,c|char|1|14,d|char|1|12,e|char|1|13";
	if(!a.project(dict))
	    std::cout <<"error: projection failed with: " << dict << std::endl;
	std::cout << "after project: sizeof AAA="<<a.size()<<", a.a=" << a.a() << 
	             " a.b=" << a.b() << " a.c=" << (int)a.c() <<std::endl;
	a.rebase(aa.base<char>()+aa.size());
	std::cout << "after rebase: a.a=" << a.a() << 
	             " a.b=" << a.b() << " a.c=" << (int)a.c() <<std::endl;
	
	char buf2[1024] = {};
	a.rebase(buf);
	AAA aaa(buf2, a);
	print_hex("original", buf, 64);
	std::cout << "copy: aaa.a=" << aaa.a() << 
	             " aaa.b=" << aaa.b() << " aaa.c=" << (int)aaa.c() <<std::endl;
	print_hex("copy", buf2, 64);
	
				 
	BBB b(buf);
	const char * dict2 = "!BBB:AAA|1|0|4,a|int|4|16,b|int|4|20,c|int|40|24";
	const char * dict3 = BBB::dict();
	if(!b.project(dict2))
	    std::cout <<"error: projection failed with: " << dict2 << std::endl;
	b.b_() = 15;
	std::cout << "b.a=" << b.a() <<", b.b=" << b.b() <<", b.AAA::b=" << b.AAA::b() << std::endl;
	std::cout << "&buf="<< (void*)buf<<", address of b.a="<<&b.a_()<<", &b.AAA::a="<<&b.AAA::a_()<< std::endl;
	//std::cout << "&b.a using b.a()="<<&b.a()<< std::endl; //error - needs lvalue to obtain address
}
	
	uint32_t max_loop = 1000000000;
	
{
	C &c = *reinterpret_cast<C*>(buf);;
	auto start = std::chrono::system_clock::now();
	for(long i = 0; i < max_loop; ++i)
	{
		c.a.a.a += ((i<<62)>>62);
		c.a.a.b += c.a.a.a+3;
		//a.a_() = i;
		//a.b_() = i+1;
	}
	std::chrono::duration<double> diff = (std::chrono::system_clock::now() - start);
	std::cout << "native struct: " << max_loop <<" loop took " << diff.count() <<" secs" << std::endl;
	std::cout << " c.a.a.a=" << c.a.a.a << " c.a.a.b=" << c.a.a.b <<std::endl;
}
	
{
	CCC c(buf);
	auto start = std::chrono::system_clock::now();
	for(long i = 0; i < max_loop; ++i)
	{
		c.a_().a_() += ((i<<62)>>62);
		c.a_().b_() += c.a().a()+3;
		//a.a_() = i;
		//a.b_() = i+1;
	}
	std::chrono::duration<double> diff = (std::chrono::system_clock::now() - start);
	std::cout << "dv struct: " << max_loop <<" loop took " << diff.count() <<" secs" << std::endl;
	std::cout << " c.a.a=" << c.a().a() << " c.a.b=" << c.a().b() <<std::endl;
}


}