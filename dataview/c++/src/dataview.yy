%code requires
{

}

%{
    #include <iostream>
    #include <map>
    #include <set>
    #include <string>
    #include <vector>
    #include <stdint.h>
    #include <stdio.h>
    #include <stdarg.h>
    
    //void yyerror(const char *str);
    void yyerror(const char *s, ...);
    
    struct sym_table
    {
        std::string name;
        std::string type;
        uint32_t size;
        uint16_t offset;
        uint16_t n;
        bool is_udt;
    };
    
    int offset;
    std::vector<sym_table> sym_list;
    std::set<std::string> id_set;
    std::map<std::string, int> type_size;
    int aligned = 1;
    int major, minor;
    
    bool add_to_sym_table(std::string name, std::string type, size_t arr_size, bool is_udt)
    {
        if(id_set.find(name) != id_set.end())
            return false;
        sym_table st;
        st.name = name;
        st.type = type;
        st.n = arr_size;
        st.is_udt = is_udt;
        int size = type_size[type];
        st.size = st.n ? size*st.n : size;
        int room = aligned - offset % aligned;
        // adjust offset to be aligned
        (room >= size) ? 0 : offset = ((offset+aligned-1)/aligned)*aligned;
        st.offset = offset;
        offset += st.size;
        id_set.insert(st.name);
        sym_list.push_back(st);
        return true;
    }
    
    void print_project()
    {
        std::cout <<
"    void project() {" << std::endl;
        for(int i = 0; i < sym_list.size(); ++i) {
            const sym_table & st = sym_list[i];
            const std::string & type = st.type;
            const std::string & name = st.name;
            std::cout <<
"        "<<name<<"_offset__ = "<<st.offset<<";\n" 
;
        }
        std::cout<<
"    }\n"
;
    }
    
    void print_project_with_dict(const char *name)
    {
        std::cout <<
"    bool project(void *p, const char *d) {\n"
"        base = p;\n"
"        std::vector<std::string> dic_table;\n"
"        DataViewHelper::split(d, ',', dic_table);\n"
"        for(auto e : dic_table) {\n"
"            std::vector<std::string> items;\n"
"            DataViewHelper::split(e, '|', items);\n"
"            if(items[0][0] == '!') {\n"
"                if(items[0] != \"!"<<name<<"\") \n"
"                    return false;\n"
"                if(std::stoi(items[1]) == "<<major<<") {\n"
"                    project();\n"
"                    return true;\n"
"                }\n"
"            }\n"
;
        for(int i = 0; i < sym_list.size(); ++i) {
            const sym_table & st = sym_list[i];
            const std::string & type = st.type;
            const std::string & name = st.name;
            std::cout <<
"             if(items[0] == \""<<name<<"\" && items[1] == \""<<type<<"\") {\n"
"                 "<<name<<"_offset__ = std::stoi(items[3]);\n"
"             }\n"
;
        }
        std::cout<<
"        }\n"
"        return true;\n"
"    }\n"
;
    }
    
    void print_vars()
    {
        for(int i = 0; i < sym_list.size(); ++i) {
            const sym_table & st = sym_list[i];
            const std::string & type = st.type;
            const std::string & name = st.name;

            std::cout<<
"    uint16_t "<<name<<"_offset__ = "<<st.offset<<";\n";
            if(!st.is_udt) {
                if(!st.n) {
                    std::cout<<
"    "<<type<<" "<<name<<"() const { return *DataViewHelper::f_"<<type<<"(base, "<<name<<"_offset__); }\n"
"    "<<type<<"& "<<name<<"_() { return *DataViewHelper::f_"<<type<<"(base, "<<name<<"_offset__); }\n"
;
                }
                else {
                    std::cout<<
"    const "<<type<<"* "<<name<<"() const { return DataViewHelper::f_"<<type<<"(base, "<<name<<"_offset__); }\n"
"    "<<type<<"* "<<name<<"_() { return DataViewHelper::f_"<<type<<"(base, "<<name<<"_offset__); }\n"
;
                }
            }
            else {
                std::cout<<
"    "<<type<<" __"<<name<<"__;\n"
"    const "<<type<<" & "<<name<<"() const { return __"<<name<<"__; }\n"
"    "<<type<<" & "<<name<<"_() { return __"<<name<<"__; }\n"
;
            }
        }
    }

    void print_dict(const char * sname)
    {
        // dictionary string
        std::cout<<
"    const char * dict = \"!"<<sname<<"|"<<major<<"|"<<minor;
        for(int i = 0; i < sym_list.size(); ++i) {
            const sym_table & st = sym_list[i];
            const std::string & type = st.type;
            const std::string & name = st.name;
            std::cout<<","<<name<<"|"<<type<<"|"<<st.size<<"|"<<st.offset;
        }
        std::cout << "\";\n";
    }
    
    void print_initializer()
    {
        for(int i = 0; i < sym_list.size(); ++i) {
            const sym_table & st = sym_list[i];
            const std::string & type = st.type;
            const std::string & name = st.name;
            if(st.is_udt) {
                std::cout <<", __"<<name<<"__(reinterpret_cast<char*>(base)+"<<name<<"_offset__)";
            }
        }
    }

    void print_struct(const char * name)
    {
        std::cout <<
"struct "<<name<<"\n"
"{\n"
"    void * base;\n"
"    "<<name<<"(void *b) : base(b)"
;
        print_initializer();
        std::cout << "\n"
"    {}\n"
;
        print_dict(name);
        print_vars();
        print_project();
        print_project_with_dict(name);
        
        std::cout <<
"};\n\n"
;
    }

    void print_struct(const char * name, const char * parent)
    {
        std::cout <<
"struct "<<name<<" : public " << parent <<"\n"
"{\n"
"    void * base;\n"
"    "<<name<<"(void *b) : "<<parent<<"(b)"
;
        print_initializer();
        std::cout << "\n"
"    {}\n"
;
        print_dict(name);
        print_vars();
        print_project();
        print_project_with_dict(name);
        
        std::cout <<
"};\n\n"
;
    }

    void print_helper()
    {
        std::cout<<
"#include <iostream>\n"
"#include <sstream>\n"
"#include <string>\n"
"#include <vector>\n"
"\n"
"struct DataViewHelper {\n"
"    static int* f_int(void* p, uint16_t offset) {return reinterpret_cast<int*>(reinterpret_cast<char*>(p)+offset); }\n"
"    static short* f_short(void* p, uint16_t offset) {return reinterpret_cast<short*>(reinterpret_cast<char*>(p)+offset); }\n"
"    static char* f_char(void* p, uint16_t offset) {return (reinterpret_cast<char*>(p)+offset); }\n"
"    static long* f_long(void* p, uint16_t offset) {return reinterpret_cast<long*>(reinterpret_cast<char*>(p)+offset); }\n"
"    static void split(const std::string &s, char delim, std::vector<std::string> &elems) {\n"
"        std::stringstream ss(s);\n"
"        std::string item;\n"
"        while (std::getline(ss, item, delim)) {\n"
"            elems.push_back(item);\n"
"        }\n"
"    }\n"
"};\n\n";
    }
    
///////////////////////////////////////////////
    int yylex();
    int yyparse();
    //void yyerror(const char *str)
    //{
    //    fprintf(stderr, "error: %s\n", str);
    //}
    void yyerror(const char *s, ...)
    {
        va_list ap;
        va_start(ap, s);

        fprintf(stderr, "error: ");
        vfprintf(stderr, s, ap);
        fprintf(stderr, "\n");
    }
    int main()
    {
        type_size["char"] = 1;
        type_size["short"] = 2;
        type_size["int"] = 4;
        type_size["long"] = 8;
        
        print_helper();
        
        yyparse();
        return 0;
    }
    
%}

%union
{
    const char * cstr;
    int val;
}

%token STRUCT OBLOCK CBLOCK OB CB INT CHAR LONG SHORT SEMICOLON COLON DIR_ALIGNED DIR_VERSION
%token <cstr> IDENTIFIER
%token <val> NUMBER
%type <cstr> type identifier user_type

%%

data_structures: 
    DIR_VERSION NUMBER NUMBER {
        major = $2; minor = $3;
    }
    directives structures
 ;
 
directives: /* empty */
 | directives directive
;

directive: 
    DIR_ALIGNED NUMBER { 
        if($2 == 0) { 
            yyerror("invalid alignment for \%aligned"); 
            exit(1);
        }
        aligned = $2;
    }
 ;
 
structures: structure
 | structures structure
;

structure: 
    STRUCT IDENTIFIER { 
        offset = 0;
        if(type_size.find($2) != type_size.end()) {
            yyerror("duplicated struct name: %s", $2);
            exit(1);
        }
    }
    OBLOCK 
    variable_defs
    CBLOCK {
        offset = ((offset+aligned-1)/aligned)*aligned;
        print_struct($2);
        type_size[$2] = offset; offset = 0; 
        id_set.clear(); 
        sym_list.clear();
    }
 |  STRUCT IDENTIFIER COLON IDENTIFIER {
        if(type_size.find($2) != type_size.end()) {
            yyerror("duplicated struct name: %s", $2);
            exit(1);
        }
        offset = type_size[$4];
    }
    OBLOCK 
    variable_defs
    CBLOCK {
        offset = ((offset+aligned-1)/aligned)*aligned;
        print_struct($2, $4);
        type_size[$2] = offset; offset = 0; 
        id_set.clear(); 
        sym_list.clear();
    }
;

variable_defs: variable_def
 | variable_defs variable_def
 ;
 
variable_def:
    type identifier SEMICOLON {
        if(!add_to_sym_table($2, $1, 0, false)) {
            yyerror("duplicated identifier : %s", $2);
            exit(1);
        }
    }
 |  type identifier OB NUMBER CB SEMICOLON {
        if(!add_to_sym_table($2, $1, 4, false)) {
            yyerror("duplicated identifier : %s", $2);
            exit(1);
        } 
    }
 | user_type identifier SEMICOLON {
        if(!add_to_sym_table($2, $1, 0, true)) {
            yyerror("duplicated identifier : %s", $2);
            exit(1);
        } 
    }
;

type: INT   { $$ = "int"; }
 | CHAR     { $$ = "char"; }
 | SHORT    { $$ = "short"; }
 | LONG     { $$ = "long"; }
 ;
 
user_type: identifier
;

identifier: IDENTIFIER { $$ = $1; }
;
%%
        
