//
// Created by jk on 1/24/16.
//

#ifndef BIGLITTLE_DATAGRAMSOCKETLISTENER_H
#define BIGLITTLE_DATAGRAMSOCKETLISTENER_H

#include <string>
#include <vector>

#include <SocketListener.h>

template<int SIZE>
class DatagramServerSocketListener : public DatagramSocketListener<DatagramServerSocketListener<SIZE>>
{
    char buf[SIZE];
    DataProcessor *processor;

public:
    DatagramServerSocketListener(uint16_t port)
            : DatagramSocketListener<DatagramServerSocketListener<SIZE>>(port), processor(nullptr)
    {
        this->addr.sin_addr.s_addr = htonl(INADDR_ANY);
        if (bind(this->fd, (struct sockaddr *)&this->addr, sizeof(this->addr)) < 0) {
            throw "bind failed";
        }
    }

    void setDataProcessor(DataProcessor * proc) {
        processor = proc;
    }

    virtual int operator()() override {
        struct sockaddr_in remote_addr;
        socklen_t addr_len = sizeof(remote_addr);
        ssize_t len = recvfrom(this->fd, buf, sizeof(buf), 0, (struct sockaddr *)&remote_addr, &addr_len);
        if(len <= 0)
            return this->fd;
        if(processor != nullptr)
            processor->operator()(buf, sizeof(buf), len);
        //printf("subscription ip = %s\n", buf);
        return this->fd;
    }
};

template<int SIZE>
class MulticastServerSocketListener : public DatagramServerSocketListener<SIZE> {

public:
    MulticastServerSocketListener(std::string local, std::string mcastAddr, uint16_t port)
            : DatagramServerSocketListener<SIZE>(port)
    {
        struct ip_mreq group;
        group.imr_multiaddr.s_addr = inet_addr(mcastAddr.c_str());
        group.imr_interface.s_addr = inet_addr(local.c_str());
        if(setsockopt(this->fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0)
        {
            std::cerr << "Multicast subscription error = " << errno << std::endl;
            throw "Adding multicast group error";
        }
        else
            printf("Adding multicast group...OK.\n");
    }
};

class DatagramClientSocketListener : public DatagramSocketListener<DatagramClientSocketListener> {
    socklen_t addr_len = sizeof(addr);
    std::string address;
    uint16_t port;

public:
    DatagramClientSocketListener(uint16_t port_)
            : DatagramSocketListener<DatagramClientSocketListener>(port_), port(port_)
    {}

    virtual int operator()() {
        return fd;
    }

    int setServerAddress(std::string newAddr) {
        address = newAddr;
        printf("new address = %s\n", address.c_str());
        struct hostent *server;
        // gethostbyname: get the server's DNS entry
        server = gethostbyname(address.c_str());
        if (server == NULL) {
            perror("ERROR, no such hostname");
            return -1;
        }
        // build the server's Internet address
        bzero((char *) &addr, sizeof(addr));
        addr.sin_family = AF_INET;
        bcopy((char *)server->h_addr,
              (char *)&addr.sin_addr.s_addr, server->h_length);
        addr.sin_port = htons(port);
    }

    ssize_t send(void* buf, size_t len) {
        if(fd < 0 || address.empty())
            return -1;

        ssize_t n = sendto(fd, buf, len, 0, (const struct sockaddr *)&addr, addr_len);
        if (n < 0)
            perror("ERROR in sendto");
        return n;
    }
};

#endif //BIGLITTLE_DATAGRAMSOCKETLISTENER_H
