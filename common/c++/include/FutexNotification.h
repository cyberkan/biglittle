class FutexNotificationImpl : public InterProcessNotification
{
    int wait() {
        return syscall(SYS_futex, pfutex_, FUTEX_WAIT, *pfutex_, (void*)0, (void*)0, 0);
    }
    
    int timedwait(uint32_t timeout_usec) {
        struct timespec timeout = { 0, 0 };
        timeout.tv_nsec = timeout.tv_nsec + timeout_usec * 1000;
        return syscall(SYS_futex, pfutex_, FUTEX_WAIT, *pfutex_, &timeout, (void*)0, 0);
    }
    int signal() {
        return syscall(SYS_futex, pfutex_, FUTEX_WAKE, 1, (void*)0, (void*)0, 0);
    }
    int broadcast() {
        return syscall(SYS_futex, pfutex_, FUTEX_WAKE, INT_MAX, (void*)0, (void*)0, 0);
    }
};