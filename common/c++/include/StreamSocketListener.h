//
// Created by jk on 1/24/16.
//

#ifndef BIGLITTLE_STREAMSOCKETLISTENER_H
#define BIGLITTLE_STREAMSOCKETLISTENER_H

#include <string>
#include <vector>
#include <algorithm>

#include <SocketListener.h>
#include <DataProcessor.h>
#include <Multiplexer.h>

template<int SIZE>
class StreamUserSocketListener : public StreamSocketListener<StreamUserSocketListener<SIZE>>
{
    char buf[SIZE];
    DataProcessor *processor;

public:
    StreamUserSocketListener(int fd_)
            : StreamSocketListener<StreamUserSocketListener<SIZE>>(fd_) {}

    StreamUserSocketListener(uint16_t port)
            : StreamSocketListener<StreamUserSocketListener<SIZE>>(port) {}

    void setDataProcessor(DataProcessor *proc) {
        processor = proc;
    }

    int operator()() override {
        ssize_t len = read(this->fd, buf, sizeof(buf));
        if(len == 0) { // possible the remote is closed
            std::cout << "received 0 length data from " << this->fd << std::endl;
            shutdown();
            close(this->fd);
            this->fd = -1;
            return this->fd;
        }
        if(processor != nullptr)
            processor->operator()(buf, sizeof(buf), len);

        return this->fd;
    }
    void shutdown() {
        std::cout << "shutting down socket " << this->fd << std::endl;
        if(processor != nullptr)
            processor->shutdown();
    }
};

template<int SIZE>
class StreamServerSocketListener : public StreamSocketListener<StreamServerSocketListener<SIZE>>
{
public:
    typedef StreamUserSocketListener<SIZE> user_type;

private:
    std::vector<user_type> sockets;
    std::vector<CorrelatedDataProcessor<user_type> *> processors;
    struct sockaddr_in remote_addr;
    Multiplexer *selector = nullptr;
    enum {
        MAXCONN = 5
    };

public:

    StreamServerSocketListener(uint16_t port)
            : StreamSocketListener<StreamServerSocketListener<SIZE>>(port)
    {
        this->setOptions(SO_REUSEADDR, true);
        this->addr.sin_addr.s_addr = htonl(INADDR_ANY);
        if (bind(this->fd, (struct sockaddr *)&this->addr, sizeof(this->addr)) < 0)
            throw "bind failed";
        if (listen(this->fd, MAXCONN) < 0)
            throw "listen() failed";
    }
    StreamServerSocketListener(Multiplexer * mux, uint16_t port)
            : StreamServerSocketListener(port)
    {
        selector = mux;
    }

    void setDataProcessor(std::vector<CorrelatedDataProcessor<user_type> *> proc) {
        processors = proc;
    }

    void setDataProcessor(CorrelatedDataProcessor<user_type> ** proc, size_t num) {
        for(size_t i = 0; i < num; ++i)
            processors.push_back(*proc++);
    }

    virtual int operator()() override {

        socklen_t addr_len = sizeof(remote_addr);
        int newfd = accept(this->fd, (struct sockaddr *)&remote_addr, &addr_len);
        if(newfd < 0)
            return this->fd;
        //auto it = std::find_if(sockets.begin(), sockets.end(),
        //                       [] (user_type& x) {return (-1 == x.getfd()) ? true : false;} );
        auto it = sockets.begin();
        for(; it != sockets.end(); ++it) {
            std::cout << "socket fd = " << it->getfd() << std::endl;
            if(it->getfd() == -1)
                break;
        }
        if(it != sockets.end()) {
            std::cout << "replacing FD with new one " << newfd << std::endl;
            it->shutdown();
            it->resetSocket(newfd);
            if(selector != nullptr)
                selector->add(*it);
        }
        else if(processors.size() > 0) {
            std::cout << "adding new socket " << newfd << std::endl;
            user_type new_sock(newfd);
            sockets.push_back(new_sock);
            processors.back()->setCorrelator(&sockets.back());
            sockets.back().setDataProcessor(processors.back());
            processors.pop_back();
            if(selector != nullptr)
                selector->add(sockets.back());
        }
        else {
            close(newfd);
            std::cerr << "exceed the maxinum number of connections\n";
        }
        return this->fd;
    }

    virtual int recover() override {
        return this->fd;
    }
};

template<int SIZE>
class StreamClientSocketListener : public StreamUserSocketListener<SIZE>
{

public:
    StreamClientSocketListener(std::string address, uint16_t port)
            : StreamUserSocketListener<SIZE>(port)
    {
        this->addr.sin_addr.s_addr = inet_addr(address.c_str());
        int ret = connect(this->fd, (const struct sockaddr *) &this->addr, sizeof(this->addr));
        if(ret < 0) {
            std::cerr << "connect() failed, errno = " << errno << std::endl;
            throw "connect() failed";
        }
    }
};

#endif //BIGLITTLE_STREAMSOCKETLISTENER_H
