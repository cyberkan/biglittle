//
// Created by jk on 1/24/16.
//

#ifndef BIGLITTLE_DATAPROCESSOR_H
#define BIGLITTLE_DATAPROCESSOR_H

class DataProcessor
{
public:
	virtual void operator()(char * buf, size_t buf_len, size_t len) = 0;
	virtual void shutdown() {
	}
};

template<typename T>
class CorrelatedDataProcessor : public DataProcessor
{
protected:
	T *correlator = nullptr;

public:
	void setCorrelator(T * cor) {
		correlator = cor;
	}
};


#endif //BIGLITTLE_DATAPROCESSOR_H
