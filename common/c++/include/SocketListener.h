//
// Created by jk on 1/24/16.
//

#ifndef BIGLITTLE_SOCKETLISTENER_H
#define BIGLITTLE_SOCKETLISTENER_H

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <FileDescriptorListener.h>

class SocketListener : public FileDescriptorListener
{
protected:
    struct sockaddr_in addr;

public:
    SocketListener(uint16_t port, int type, int proto) {
        fd = socket(AF_INET, type, proto);
        if(fd < 0) {
            throw "ERROR : fails in opening socket";
        }
        memset((char *)&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
    }

    SocketListener(int fd_) : FileDescriptorListener(fd_) {
        if (!loadAddressInfo()) {
            throw "can't find address infor for the given socket descriptor";
        }
    }
    void resetSocket(int fd_) {
        if(this->fd != -1)
            close(this->fd);
        setfd(fd_);
        if (!loadAddressInfo()) {
            std::cerr << "can't find address infor for the given socket descriptor\n";
        }
    }

private:
    bool loadAddressInfo() {
        socklen_t len = sizeof(addr);
        if (getsockname(fd, (struct sockaddr *)&addr, &len) < 0) {
            return false;
        }
        return true;

    }

};

template <typename DERIVED>
class ControllableSocketListener : public SocketListener
{
public:
    ControllableSocketListener(uint16_t port, int type, int proto) : SocketListener(port, type, proto) {
    }
    ControllableSocketListener(int fd_) : SocketListener(fd_) {
    }
    DERIVED & setOptions(int opt, bool on) {
        int optval = on ? 1 : 0;
        if(setsockopt(fd, SOL_SOCKET, opt, &optval, sizeof(optval)) < 0) {
            printf("setsockopt() failed errno=%d\n", errno);
        }
        return *reinterpret_cast<DERIVED*>(this);
    }
};

template<typename DERIVED>
class StreamSocketListener : public ControllableSocketListener<DERIVED>
{
public:
    StreamSocketListener(uint16_t port) : ControllableSocketListener<DERIVED>(port, SOCK_STREAM, IPPROTO_TCP) {
    }
    StreamSocketListener(int fd_) : ControllableSocketListener<DERIVED>(fd_) {
    }
};

template<typename DERIVED>
class DatagramSocketListener : public ControllableSocketListener<DERIVED>
{
public:
    DatagramSocketListener(uint16_t port) : ControllableSocketListener<DERIVED>(port, SOCK_DGRAM, 0) {
    }
};

#endif //BIGLITTLE_SOCKETLISTENER_H
