///////////////////////////////
// rcs_id = "$Id$"
///////////////////////////////

#ifndef __THREADCPUAFFINITY_H__
#define __THREADCPUAFFINITY_H__

#include <thread>
#include <pthread.h>
#include <unistd.h>

namespace jk {
    class thread : public std::thread
    {
    public:
        thread(thread&) = delete;
        thread() : std::thread() {}
        thread(thread&& t) : std::thread(static_cast<std::thread&&>(t)) {}
        template< typename Function, typename... Args >
        explicit thread( Function&& f, Args&&... args ) : std::thread(f, args...) {};
        
        template<typename... ARGS>
        bool setcpu(uint16_t cpu, ARGS... args)
        {
            cpu_set_t cur_cpu;
            CPU_ZERO(&cur_cpu);
            setcpu_(cur_cpu, cpu, args...);
            return 0 == pthread_setaffinity_np(native_handle(), sizeof(cur_cpu), &cur_cpu);
        }

        template<typename... ARGS>
        bool addcpu(uint16_t cpu, ARGS... args)
        {
            cpu_set_t cur_cpu;
            int rs = pthread_getaffinity_np(native_handle(), sizeof(cur_cpu), &cur_cpu);
            if(rs)
                return false;
            setcpu_(cur_cpu, cpu, args...);
            return 0 == pthread_setaffinity_np(native_handle(), sizeof(cur_cpu), &cur_cpu);
        }
        bool resetcpu()
        {
            cpu_set_t cur_cpu;
            CPU_ZERO(&cur_cpu);
            return 0 == pthread_setaffinity_np(native_handle(), sizeof(cur_cpu), &cur_cpu);            
        }
    private:
        template<typename... ARGS> 
        void setcpu_(cpu_set_t& cpuset, uint16_t cpu, ARGS... args)
        {
            CPU_SET(cpu, &cpuset);
            setcpu_(cpuset, args...);
            return;
        }
        void setcpu_(cpu_set_t& cpuset, uint16_t cpu)
        {
            CPU_SET(cpu, &cpuset);
            return;
        }
    };
}

#endif //__THREADCPUAFFINITY_H__