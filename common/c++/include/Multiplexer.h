//
// Created by jk on 1/24/16.
//

#ifndef BIGLITTLE_MULTIPLEXER_H
#define BIGLITTLE_MULTIPLEXER_H

#include <sys/poll.h>

#include <vector>
#include <map>
#include <atomic>

class Multiplexer {
private:
    std::vector<struct pollfd> fds;
    std::vector<struct pollfd> newfds;
    std::map<int, FileDescriptorListener*> listeners;
    std::atomic_flag lock = ATOMIC_FLAG_INIT;

public:

    void add(FileDescriptorListener & fd) {
        struct pollfd pfd;
        pfd.fd = fd.getfd();
        pfd.events = POLLIN;
        std::cout << "added new fd = " << pfd.fd << " to multiplexer\n";
        while(!lock.test_and_set());
        newfds.push_back(pfd);
        if(!listeners.insert(std::pair<int, FileDescriptorListener*>(fd.getfd(), &fd)).second) {
            //std::cerr << "failed to add to listener map\n";
            listeners[fd.getfd()] = &fd;
        }
        lock.clear();
    }

    int poll() {
        return poll(0);
    }

    int poll(int timeout) {
        processNewFDs();
        int ret;
        do {
            ret = ::poll(fds.data(), fds.size(), timeout);
        } while(ret < 0 && errno == EINTR);
        if(ret == 0)
            return ret;
        if(ret < 0) {
            printf("Error in poll() : errno=%d\n", errno);
            return ret;
        }

        for(auto & pfd : fds) {
            if(pfd.revents == POLLIN) {
                pfd.fd = listeners[pfd.fd]->operator()();
            }
            else if(pfd.revents == POLLERR ||
                    pfd.revents == POLLHUP ||
                    pfd.revents == POLLNVAL) {
                pfd.fd = listeners[pfd.fd]->recover();
            }
        }
        return ret;
    }

private:
    void processNewFDs() {
        while(!lock.test_and_set());
        // remove invalid fds
        for(auto & pfd : fds) {
            if(pfd.fd < 0) {
                pfd = fds.back();
                fds.pop_back();
            }
        }
        // add new fds
        for(auto & pfd : newfds)
            fds.push_back(pfd);
        newfds.clear();
        lock.clear();
    }
};

#endif //BIGLITTLE_MULTIPLEXER_H
