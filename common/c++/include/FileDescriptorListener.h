//
// Created by jk on 1/24/16.
//

#ifndef BIGLITTLE_FILEDESCRIPTORLISTENER_H
#define BIGLITTLE_FILEDESCRIPTORLISTENER_H

#include <iostream>

#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <DataProcessor.h>

class FileDescriptorListener {
protected:
    int fd = -1;

public:
    FileDescriptorListener() {
    }
    FileDescriptorListener(int fd_) : fd(fd_) {
    }
    FileDescriptorListener& setNonblock(bool nonblock=true) {
        int opt = nonblock ? 1 : 0;
        if(ioctl(fd, FIONBIO, &opt) < 0)
            printf("setting to non-block failed");
        return *this;
    }
    int getfd() {
        return fd;
    }
    void setfd(int fd_) {
        fd = fd_;
    }
    virtual int operator()() = 0;
    virtual int recover() {
        std::cerr << "Try to recover on fd=" << fd << std::endl;
        if(fd >= 0)
            close(fd);
        fd = -1;
        return fd;
    }
};


#endif //BIGLITTLE_FILEDESCRIPTORLISTENER_H
