///////////////////////////////
// rcs_id = "$Id$"
///////////////////////////////

#ifndef __TEMPLATEUTIL_H__
#define __TEMPLATEUTIL_H__

#include <type_traits>

template <uint32_t BASE, uint32_t N>
struct POWER 
{ 
    enum { value = BASE * BASE * POWER<BASE,N-2>::value };
};

template <uint32_t BASE>
struct POWER<BASE, 0>
{
    enum {value = 1};
};

template <uint32_t BASE>
struct POWER<BASE, 1>
{
    enum {value = BASE};
};

template <uint32_t BASE>
struct POWER<BASE, 2>
{
    enum {value = BASE * BASE};
};

template <uint32_t N>
struct CeilToPow2_
{
    enum {value = 2 * CeilToPow2_<(N>>1)>::value};
};

template <>
struct CeilToPow2_<0>
{
    enum {value = 1};
};

template <uint32_t N>
struct CeilToPow2
{
    enum {value = CeilToPow2_<N-1>::value};
};

template <>
struct CeilToPow2<1>
{
    enum {value = 2};
};

template <>
struct CeilToPow2<0>
{
    enum {value = 2};
};

#endif //__TEMPLATEUTIL_H__