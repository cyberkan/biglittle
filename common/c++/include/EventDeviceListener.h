//
// Created by jk on 1/24/16.
//

#ifndef BIGLITTLE_EVENTDEVICELISTENER_H
#define BIGLITTLE_EVENTDEVICELISTENER_H

#include <fcntl.h>
#include <linux/input.h>

#include <string>

#include <FileDescriptorListener.h>

class EventDeviceListener : public FileDescriptorListener {
    std::string device;
    DataProcessor *processor;

public:
    EventDeviceListener(std::string name) : processor(nullptr) {
        device = name;
        if ((getuid ()) != 0)
            printf ("You are not root! This may not work...\n");

        //Open Device
        if ((fd = open (device.c_str(), O_RDONLY)) == -1) {
            printf ("%s is not a vaild device interface.\n", device.c_str());
            return;
        }
        printf ("getting detail information for device...\n");
        char device_info[1024];
        ::ioctl (fd, EVIOCGNAME (sizeof (device_info)), device_info);
        printf ("connected device : %s (%s)\n", device.c_str(), device_info);
    }

    void setDataProcessor(DataProcessor * proc) {
        processor = proc;
    }

    virtual int operator()() override {
        if(fd < 0) {
            return fd;
        }
        struct input_event ev[64];
        ssize_t len;
        if ((len = read (fd, ev, sizeof(struct input_event)*1)) < sizeof(struct input_event)) {
            perror("read()");
            return fd;
        }
        if(len <= 0)
            return fd;
        if(processor != nullptr)
            processor->operator()((char*)ev, sizeof(struct input_event), sizeof(struct input_event));
        //printf ("%d Type[%d] Code[%04x] Value[%08x]\n", len, ev[0].type, ev[0].code, ev[0].value);
        return fd;
    }

    virtual int recover() override {
        std::cout << "try to recover...\n";
        close(fd);
        //re-Open Device
        int newfd;
        if ((newfd = open (device.c_str(), O_RDONLY)) == -1) {
            printf ("%s is not a vaild device interface, errno=%d\n", device.c_str(), errno);
            newfd = fd;
        }
        fd = newfd;
        return fd;
    }
};

#endif //BIGLITTLE_EVENTDEVICELISTENER_H
