#include <unistd.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sys/poll.h>
#include <sys/select.h>
#include <termios.h>

#include <iostream>
#include <vector>
#include <map>
#include <atomic>

class FileDescriptorListener {
protected:
    int fd = -1;

public:
    FileDescriptorListener() {
    }
    FileDescriptorListener(int fd_) : fd(fd_) {
    }
    FileDescriptorListener& setNonblock(bool nonblock=true) {
        int opt = nonblock ? 1 : 0;
        if(ioctl(fd, FIONBIO, &opt) < 0)
            printf("setting to non-block failed");
        return *this;
    }
    int getfd() {
        return fd;
    }
    void setfd(int fd_) {
        fd = fd_;
    }
    virtual int operator()() = 0;
    virtual int recover() {
        if(fd >= 0)
            close(this->fd);
        this->fd = -1;
        return this->fd;
    }
};

class DataProcessor
{
public:
    virtual void operator()(char * buf, size_t buf_len, size_t len) = 0;
    virtual void shutdown() {
    }
};

template<typename T>
class CorrelatedDataProcessor : public DataProcessor
{
protected:
	T *correlator = nullptr;
	
public:
	void setCorrelator(T * cor) {
		correlator = cor;
	}
};

class Multiplexer {
private:
    std::vector<struct pollfd> fds;
    std::vector<struct pollfd> newfds;
    std::map<int, FileDescriptorListener*> listeners;
    std::atomic_flag lock = ATOMIC_FLAG_INIT;

public:

    void operator()() {
    }

    void add(FileDescriptorListener & fd) {
        struct pollfd pfd;
        pfd.fd = fd.getfd();
        pfd.events = POLLIN;
        while(!lock.test_and_set());
        newfds.push_back(pfd);
        listeners.insert(std::pair<int, FileDescriptorListener*>(fd.getfd(), &fd));
        lock.clear();
    }

    int poll() {
        return poll(0);
    }

    int poll(int timeout) {
        processNewFDs();
        int ret;
        do {
            ret = ::poll(fds.data(), fds.size(), timeout);
        } while(ret < 0 && errno == EINTR);
        if(ret == 0)
            return ret;
        if(ret < 0) {
            printf("Error in poll() : errno=%d\n", errno);
            return ret;
        }

        for(struct pollfd& pfd : fds) {
            if(pfd.revents == POLLIN) {
                pfd.fd = listeners[pfd.fd]->operator()();
            }
            else if(pfd.revents == POLLERR ||
                    pfd.revents == POLLHUP ||
                    pfd.revents == POLLNVAL) {
                pfd.fd = listeners[pfd.fd]->recover();
            }
        }
        return ret;
    }

private:
    void processNewFDs() {
        if(newfds.size() == 0)
            return;
        while(!lock.test_and_set());
        // remove invalid fds
        for(struct pollfd & pfd : fds) {
            if(pfd.fd < 0) {
                pfd = fds.back();
                fds.pop_back();
            }
        }
        // add new fds
        for(struct pollfd pfd : newfds)
            fds.push_back(pfd);
        newfds.clear();
        lock.clear();
    }
};

class ConsoleInput : public FileDescriptorListener
{
public:
    enum  {
        Block = 0x01,
        Echo = 0x02
    };
private:

	struct TermIOSetter 
	{
		virtual void operator()(int flags, struct termios & attr) const = 0;
	};
	struct TermIOEnabler : public TermIOSetter
	{
		void operator()(int flags, struct termios & attr) const {
			if(flags & Block) {
				attr.c_lflag |= ICANON;
				attr.c_cc[VMIN] = 1;
				attr.c_cc[VTIME] = 10;
			}
			if(flags & Echo)
				attr.c_lflag |= ECHO;
		}
	};
	struct TermIODisabler : public TermIOSetter
	{
		void operator()(int flags, struct termios & attr) const {
			if(flags & Block) {
				attr.c_lflag &= ~ICANON;
				attr.c_cc[VMIN] = 0;
				attr.c_cc[VTIME] = 0;
			}
			if(flags & Echo)
				attr.c_lflag &= ~ECHO;
		}
	};
	
public:
	ConsoleInput() : FileDescriptorListener(STDIN_FILENO) {}
	ConsoleInput & enable(int opt) {
		return setOptions(opt, TermIOEnabler());
	}
	ConsoleInput & disable(int opt) {
		return setOptions(opt, TermIODisabler());
	}
	
	int operator()() override {
		char c;
		int ret = read(fd, &c, sizeof(c));
		if(ret > 0)
			std::cout << c << std::flush;
		return fd;
	}
	int kbhit() {
		struct pollfd pfds;
		pfds.fd = 0;
		pfds.events = POLLIN;
		return poll(&pfds, 1, 0);
	}
	
private:
	ConsoleInput & setOptions(int opt, const TermIOSetter & setter) {
		struct termios attr;
		int ret = tcgetattr(fd, &attr);
		if(ret < 0)
			std::cerr << "tcgetattr() failed with fd=" << fd << std::endl;
		setter(opt, attr);
		ret = tcsetattr(fd, TCSANOW, &attr);
		if(ret < 0)
			std::cerr << "tcsetattr(TCSANOW) failed with fd=" << fd << std::endl;
		return *this;
	}
};

int main() 
{
	ConsoleInput key;
	Multiplexer selector;
	
	key.disable(key.Echo|key.Block);
	selector.add(key);
	while(1) {
		int ret = selector.poll();
		if(ret < 0)
			break;
	}
	//while (!key.kbhit());
	//key();
	key.enable(key.Echo).enable(key.Block);
}
