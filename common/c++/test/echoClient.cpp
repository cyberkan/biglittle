//
// Created by jk on 1/26/16.
//

#include <StreamSocketListener.h>
#include <DataProcessor.h>
#include <Multiplexer.h>

template<typename T>
class KeyboardInputProcessor : public CorrelatedDataProcessor<T>
{
    std::string in;
public:
    void operator()(char *buf, size_t buf_len, size_t len) override {
        for(int i = 0; i < len; ++i) {
            if(*buf == '\n') {
                write(this->correlator->getfd(), in.c_str(), in.size()+1);
                in.clear();
            }
            else
                in += *buf;
            ++buf;
        }
    }
};

class ConsoleInputListener : public FileDescriptorListener
{
    DataProcessor * processor = nullptr;
    char buf[1024];
public:
    ConsoleInputListener() : FileDescriptorListener(0) {}
    void setDataProcessor(DataProcessor * proc) {
        processor = proc;
    }
    int operator()() {
        int ret = read(fd, buf, sizeof(buf));
        if(ret > 0 && processor)
            processor->operator()(buf, sizeof(buf), ret);
        return fd;
    }
};

template<typename T>
class EchoClient : public CorrelatedDataProcessor<T>
{
public:
    void operator()(char *buf, size_t buf_len, size_t len) override {
        std::cout << "["<< buf << "]" << std::endl;
    }
    void getInput() {
        std::string in;
        std::cout << "enter : ";
        std::cin >> in;
        if(this->correlator != nullptr) {
            std::cout << "sending input=\"" << in <<"\"\n";
            write(this->correlator->getfd(), in.c_str(), in.size()+1);
        }
    }
    void shutdown() override {
        exit(1);
    }
};

int main()
{
    try {
        StreamClientSocketListener<1024> echoClient("127.0.0.1", 6703);
        EchoClient<StreamClientSocketListener<1024>> echoClientProcessor;
        ConsoleInputListener keyboard;
        KeyboardInputProcessor<StreamClientSocketListener<1024>> keyproc;
        Multiplexer selector;

        keyboard.setDataProcessor(&keyproc);
        keyproc.setCorrelator(&echoClient);
        echoClientProcessor.setCorrelator(&echoClient);
        echoClient.setDataProcessor(&echoClientProcessor);
        selector.add(echoClient);
        selector.add(keyboard);

        while(true) {
            int ret = selector.poll();
            if(ret < 0)
                break;
            //if(ret == 0)
            //    echoClientProcessor.getInput();
        }
    }
    catch (const char * e) {
        std::cerr << "Exception thrown : " << e << std::endl;
        return 1;
    }
}