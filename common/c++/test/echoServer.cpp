//
// Created by jk on 1/26/16.
//

#include <vector>

#include <StreamSocketListener.h>
#include <DataProcessor.h>
#include <Multiplexer.h>

template<typename T>
class EchoProcessor : public CorrelatedDataProcessor<T>
{
public:
    void operator()(char *buf, size_t buf_len, size_t len) override {
        std::cout << "echo : " << buf << std::endl;
        if(this->correlator != nullptr) {
            std::cout << "echoback : " << buf << std::endl;
            ssize_t ret = write(this->correlator->getfd(), buf, len);
            if(ret < 0)
                std::cerr << "write() error on " << this->correlator->getfd()
                << " with errno: " << errno << std::endl;
        }
    }
};

int main()
{
    try {
        Multiplexer selector;
        StreamServerSocketListener<1024> echoServer(&selector, 6703);
        std::vector<CorrelatedDataProcessor<StreamServerSocketListener<1024>::user_type>*> procs;
        EchoProcessor<StreamServerSocketListener<1024>::user_type> echoProcessor;

        procs.push_back(&echoProcessor);
        echoServer.setDataProcessor(procs);
        selector.add(echoServer);

        while(true) {
            int ret = selector.poll();
            if(ret < 0)
                break;
        }
    }
    catch (const char * e) {
        std::cerr << "Exception thrown : " << e << std::endl;
        return 1;
    }
}