package biglittle.test;

import biglittle.systemlib.Utility;
import biglittle.systemlib.windows.User32Lib;
import biglittle.systemlib.windows.Win_Input_C;
import biglittle.systemlib.windows.Win_KeyboardInput_C;
import com.sun.jna.platform.win32.*;

public class WindowsTest {
    private static User32Lib Lib = User32Lib.INSTANCE;

    public static void main(String[] args) throws Exception {

        testSendInputWithDirectLoading();
        testSendInputWithJNAWin();
    }

    private static void testSendInputWithDirectLoading() {
        Kernel32 kernel32 = Kernel32.INSTANCE;
        Win_KeyboardInput_C[] input = new Win_KeyboardInput_C[1];
        input[0] = new Win_KeyboardInput_C();

        System.out.println("injecting 'F'");
        Utility.usleep(5 * 1000000);
        input[0].type = Win_KeyboardInput_C.INPUT_KEYBOARD;
        input[0].wScan = 0x21;
        //input[0].wScan = 0;
        //input[0].wVk = 0x41;
        //input[0].wVk = Win_KeyboardInput_C.VK_MENU;
        input[0].time = 0;
        input[0].dwExtraInfo = 0;

        input[0].dwFlags = Win_KeyboardInput_C.KEYEVENTF_SCANCODE;
        //input[0].wVk = Win_KeyboardInput_C.VK_MENU;
        //input[0].dwFlags = 0; // key pressed
        int ret = 0;
        ret = Lib.SendInput(1, input, input[0].size());
        //input[0].wVk = 'F';
        //input[0].dwFlags = 0; // key pressed
        //Lib.SendInput(1, input, input[0].size());

        //input[0].wVk = 'F';
        //input[0].dwFlags = Win_KeyboardInput_C.KEYEVENTF_KEYUP;
        //Lib.SendInput(1, input, input[0].size());

        System.out.println("Pressed: ret="+ret+", Error="+kernel32.GetLastError());
        input[0].dwFlags = Win_KeyboardInput_C.KEYEVENTF_KEYUP | Win_KeyboardInput_C.KEYEVENTF_SCANCODE;
        //input[0].wVk = Win_KeyboardInput_C.VK_MENU;
        //input[0].dwFlags = Win_KeyboardInput_C.KEYEVENTF_KEYUP;
        ret = Lib.SendInput(1, input, input[0].size());
        System.out.println("Released: size="+input[0].size()+", ret="+ret+", Error="+kernel32.GetLastError());
    }

    private static void testSendInputWithJNAWin() {
        Kernel32 kernel32 = Kernel32.INSTANCE;
        User32 user32 = User32.INSTANCE;
        WinUser.INPUT[] ip = new WinUser.INPUT[1];
        ip[0] = new WinUser.INPUT();

        System.out.println("injecting 'A' using JNA");
        Utility.usleep(2 * 1000000);
        ip[0].type = new WinDef.DWORD(WinUser.INPUT.INPUT_KEYBOARD);
        ip[0].input.setType("ki");
        ip[0].input.ki.wScan = new WinDef.WORD(0);
        ip[0].input.ki.time = new WinDef.DWORD(0);
        ip[0].input.ki.dwExtraInfo = new BaseTSD.ULONG_PTR(0);
        ip[0].input.ki.wVk = new WinDef.WORD(0x41);

        for(int i = 0; i < 10; ++i) {
        ip[0].input.ki.dwFlags = new WinDef.DWORD(0);
        WinDef.DWORD ret = user32.SendInput(new WinDef.DWORD(1), ip, ip[0].size());
        System.out.println("Pressed: size="+ip[0].size()+", ret="+ret.intValue()+", Error="+kernel32.GetLastError());
        ip[0].input.ki.dwFlags = new WinDef.DWORD(WinUser.KEYBDINPUT.KEYEVENTF_KEYUP);
        ret = user32.SendInput(new WinDef.DWORD(1), ip, ip[0].size());
        System.out.println("Released: ret="+ret.intValue()+", Error="+kernel32.GetLastError());
        Utility.usleep(1000000);
        }
    }
    
    private static void testUnion() {
        Kernel32 kernel32 = Kernel32.INSTANCE;

        Win_Input_C.ByReference in = new Win_Input_C.ByReference();

        in.type = Win_Input_C.INPUT_KEYBOARD;
        in.in.setType("ki");
        in.in.ki.time = 0;
        in.in.ki.wScan = 0;
        in.in.ki.dwFlags = 0; // keypressed
        in.in.ki.wVk = 'F';

        int ret = Lib.SendInput(1, in, in.size());
        System.out.println("pressed: size="+in.size()+", ret="+ret+", Error="+kernel32.GetLastError());

        in.in.ki.dwFlags = Win_Input_C.KeyInput.KEYEVENTF_KEYUP; // keyup
        ret = Lib.SendInput(1, in, in.size());
        System.out.println("released: size="+in.size()+", ret="+ret+", Error="+kernel32.GetLastError());

    }
}