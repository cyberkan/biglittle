package biglittle.systemlib;

import sun.misc.Unsafe;

import java.io.IOException;
import java.lang.reflect.Field;

public class Utility {
    static private volatile long milli_second = 0;
    static private volatile long nano_second = 0;
    public static long nanoTimeDelay = -1;
    public static long currentTimeMillisDelay = -1;
    public static Unsafe UNSAFE;
    static {
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            UNSAFE = (Unsafe) field.get(null);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }
    }
    static {
        // measure  avg call time
        int test_loop = 1000000;
        long start = System.nanoTime();
        long test = 0;
        for(int i = 0; i < test_loop; ++i) {
            test += System.nanoTime();
        }
        nanoTimeDelay = (System.nanoTime() - start)/test_loop;
        start = System.nanoTime();
        for(int i = 0; i < test_loop; ++i) {
            test += System.currentTimeMillis();
        }
        currentTimeMillisDelay = (System.nanoTime() - start)/test_loop;
        //System.out.println("nanoTimeDealy="+nanoTimeDelay+", currentTimeMillis="+currentTimeMillisDelay);
        
        boolean locked = false;
        long before = 0;
        long after = 0;
        long b4milli = 0;
        long n = 1000;
        long gap = 0;
        do {
            long allowedGap = nanoTimeDelay+(n/1000)*(currentTimeMillisDelay);

            do {
                before = System.nanoTime();
                b4milli = System.currentTimeMillis();
                milli_second = System.currentTimeMillis();
                after = System.nanoTime();
            } while(before == milli_second);
            gap = after - before;
            if(gap == 0)
                continue;
            if(gap < allowedGap) {
                locked = true;
                nano_second = after;// - (nanoTimeDelay+currentTimeMillisDelay)/2;
                if (allowedGap > 1000L)
                    System.out.println("Coarsely tuned by " + allowedGap + ", n=" + n);
            }
            else 
                ++n;
        } while(!locked);
        System.out.println("Process clock is tuned by "+gap+", n="+n);
    }

    public static void msleep(long msec) {
        nanosleep(msec * 1000000);
    }
    public static void usleep(long usec) {
        nanosleep(usec * 1000);
    }
    public static void nanosleep(long nsec) {
        if(nsec > 0)
            UNSAFE.park(false, nsec);
    }

    public static long microTime() {
        return (milli_second*1000+(System.nanoTime()-nano_second)/1000);
    }

    public static long nanoTime() {
        return (milli_second*1000000+(System.nanoTime()-nano_second));
    }

    public static void pressEnter() {
        try {
            while(System.in.available() == 0);
            while(System.in.available() != 0) System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static boolean busyDelay(int factor) {
        return busyDelay(factor, 0x0FFF);
    }
    public static boolean busyDelay(int factor, int max) {
        for(int i = (factor > max) ? max : factor; i > 0; --i);
        return true;
    }
    public static long roundToPow2(long x) {
        long y = 1;
        for(int i = 1; i < Long.SIZE; ++i) {
            y <<= 1;
            if(x > y) continue;
            return y;
        }
        return y;
    }
    public static long roundTo64(long x) {
        long y = (x >> 6) ;
        return (y<<6) == x ? x : (y+1)<<6;
    }

    public static long addAndModulo(long x, long val, long limit) { // not checking overflow. caller should do before calling
        return addAndModulo(x, val, 0, limit);
    }
    public static long addAndModulo(long x, long val, long min, long max) { // not checking overflow. caller should do before calling
        long y = x+val;
        return (y >= max) ? y - max + min : y;    // faster than '%' today, but may not in the future
    }
}