package biglittle.systemlib.windows;

import biglittle.systemlib.CPUAffinity;

public class WindowsCPUAffinity implements CPUAffinity {
    private Kernel32Lib LIB = Kernel32Lib.INSTANCE;
    private int numCores = Runtime.getRuntime().availableProcessors();
    private long nativeHandle = 0;
    private long mask = 0;
    
    @Override
    public void setThreadHandle() {
        nativeHandle = LIB.GetCurrentThread();
    }

    @Override
    public void setTask() {
        if(nativeHandle != 0)
            LIB.SetThreadAffinityMask(nativeHandle, mask);
    }

    @Override
    public void setAffinity(int cpu) {
        if(numCores <= cpu) // ignore if cpu is higher than actual avilalbe cpus.
            return;
        mask |= (1 << cpu);
        setTask();
    }
}