package biglittle.systemlib.windows;

import biglittle.ipc.SemaphoreInf;
import com.sun.jna.Pointer;

public class SemaphoreWindows implements SemaphoreInf {
    private static Kernel32Lib LIB = Kernel32Lib.INSTANCE;
    Pointer handle;

    public SemaphoreWindows(String name) {
        handle = LIB.CreateSemaphoreA(null, 0L, 1L, "Local\\" + name);
    }

    @Override
    public void acquire() {
        LIB.WaitForSingleObject(handle, LIB.INFINITE);
    }

    @Override
    public void release() {
        LIB.ReleaseSemaphore(handle, 1L, null);
    }
}