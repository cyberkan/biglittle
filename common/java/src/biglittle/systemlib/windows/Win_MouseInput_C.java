package biglittle.systemlib.windows;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Dan on 12/20/2015.
 */
public class Win_MouseInput_C extends Structure {
    // type
    public static final int INPUT_MOUSE = 0;
    // mouseData
    public static final int WHEEL_DELTA                     = 120;
    public static final int XBUTTON1 = 0x0001;
    public static final int XBUTTON2 = 0x0002;
    // dwflags
    public static final int MOUSEEVENTF_ABSOLUTE        = 0x8000;
    public static final int MOUSEEVENTF_MOVE             = 0x0001;
    public static final int MOUSEEVENTF_LEFTDOWN        = 0x0002;
    public static final int MOUSEEVENTF_LEFTUP           = 0x0004;
    public static final int MOUSEEVENTF_RIGHTDOWN        = 0x0008;
    public static final int MOUSEEVENTF_RIGHTUP          = 0x0010;
    public static final int MOUSEEVENTF_MIDDLEDOWN       = 0x0020;
    public static final int MOUSEEVENTF_MIDDLEUP         = 0x0040;
    public static final int MOUSEEVENTF_VIRTUALDESK      = 0x4000;
    public static final int MOUSEEVENTF_WHEEL             = 0x0800;
    public static final int MOUSEEVENTF_XDOWN             = 0x0080;
    public static final int MOUSEEVENTF_XUP               = 0x0100;
    public static final int MOUSEEVENTF_HWHEEL            = 0x1000;
    public static final int MOUSEEVENTF_MOVE_NOCOALESCE  = 0x2000;

    //---------------------------------------
    public int type;
    public int reserved;
    public int dx;
    public int dy;
    public int mouseData;
    public int dwFlags;
    public int time;
    public long dwExtraInfo;
    //public int reserved1;
    //public int reserved2;
    //public int reserved3;
    @Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(
                "type"
                , "reserved"
                , "dx", "dy", "mouseData", "dwFlags", "time", "dwExtraInfo"
                //,"reserved1"
                //,"reserved2"
                //"reserved3"
        );
    }
    //-------------------------------------

    private static User32Lib Lib = User32Lib.INSTANCE;
    private static Win_MouseInput_C[] input = new Win_MouseInput_C[1];
    static {
        input[0] = new Win_MouseInput_C();
    }

    // for Wheel move or wheel click
    // MOUSEEVENTF_HWHEEL : mounseData
    // MOUSEEVENTF_WHEEL : mouseData
    public static void sendMouseEvent(int dwFlags, int mouseData) {
        input[0].type = Win_MouseInput_C.INPUT_MOUSE;
        input[0].dwFlags = dwFlags;
        input[0].mouseData = mouseData;
        Lib.SendInput(1, input, input[0].size());
    }
}