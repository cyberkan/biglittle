package biglittle.systemlib.windows;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.LastErrorException;
import biglittle.ipc.NamedMemoryInf;

public class SharedMemoryWindowsMmap implements NamedMemoryInf {
    private Kernel32Lib LIB = Kernel32Lib.INSTANCE;
    private final String base;
        
    public SharedMemoryWindowsMmap(String base) {
        this.base = base;
    }
    
    @Override
    public long create(String name, long memorySize, int flag) {
        int dwordMemorySize = (int) (Kernel32Lib.MAX_MEM_SIZE_2G & memorySize);
        
        String mappingObjName = "Local\\" + base + "/" + name;
        Pointer handle = LIB.CreateFileMappingA(
                null,
                null,
                Kernel32Lib.PAGE_EXECUTE_READWRITE,
                0,
                dwordMemorySize,
                mappingObjName);
        
        if (handle == null)
            throw new LastErrorException(Native.getLastError());
        Pointer mappedMemory = LIB.MapViewOfFile(handle, Kernel32Lib.FILE_MAP_ALL_ACCESS, 0, 0, dwordMemorySize);
        return Pointer.nativeValue(mappedMemory);
    }

    @Override
    public long attach(String name) {
        String mappingObjName = "Local\\" + base + "/" + name;
        Pointer handle = LIB.CreateFileMappingA(null, null, Kernel32Lib.PAGE_EXECUTE_READWRITE, 0,
                Kernel32Lib.MAX_MEM_SIZE_2G, mappingObjName);

        if (handle == null)
            throw new LastErrorException(Native.getLastError());
        Pointer mappedMemory = LIB.MapViewOfFile(handle, Kernel32Lib.FILE_MAP_ALL_ACCESS, 0, 0, 0);
        return Pointer.nativeValue(mappedMemory);
    }
}
