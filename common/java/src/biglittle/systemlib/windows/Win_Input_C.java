package biglittle.systemlib.windows;

import com.sun.jna.Structure;
import com.sun.jna.Union;

import java.util.Arrays;
import java.util.List;

public class Win_Input_C extends Structure {
    public static class KeyInput extends Structure {
        public static final int KEYEVENTF_EXTENDEDKEY = 0x0001;
        public static final int KEYEVENTF_KEYUP = 0x0002;
        public static final int KEYEVENTF_SCANCODE = 0x0008;
        public static final int KEYEVENTF_UNICODE = 0x0004;

        public short wVk;
        public short wScan;
        public int dwFlags;
        public int time;
        public long dwExtraInfo;
        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "wVk", "wScan", "dwFlags", "time", "dwExtraInfo"
            );
        }
    }
    public static class MouseInput extends Structure {
    
        // mouseData
        public static final int WHEEL_DELTA                  = 120;
        public static final int XBUTTON1                     = 0x0001;
        public static final int XBUTTON2                     = 0x0002;
        // dwflags
        public static final int MOUSEEVENTF_ABSOLUTE         = 0x8000;
        public static final int MOUSEEVENTF_MOVE             = 0x0001;
        public static final int MOUSEEVENTF_LEFTDOWN         = 0x0002;
        public static final int MOUSEEVENTF_LEFTUP           = 0x0004;
        public static final int MOUSEEVENTF_RIGHTDOWN        = 0x0008;
        public static final int MOUSEEVENTF_RIGHTUP          = 0x0010;
        public static final int MOUSEEVENTF_MIDDLEDOWN       = 0x0020;
        public static final int MOUSEEVENTF_MIDDLEUP         = 0x0040;
        public static final int MOUSEEVENTF_VIRTUALDESK      = 0x4000;
        public static final int MOUSEEVENTF_WHEEL            = 0x0800;
        public static final int MOUSEEVENTF_XDOWN            = 0x0080;
        public static final int MOUSEEVENTF_XUP              = 0x0100;
        public static final int MOUSEEVENTF_HWHEEL           = 0x1000;
        public static final int MOUSEEVENTF_MOVE_NOCOALESCE  = 0x2000;

    //---------------------------------------
    
        public int dx;
        public int dy;
        public int mouseData;
        public int dwFlags;
        public int time;
        public long dwExtraInfo;
        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(
                    "dx", "dy", "mouseData", "dwFlags", "time", "dwExtraInfo"
            );
        }
    }
    public static class Win_InputUnion extends Union {
        public static class ByValue extends Win_InputUnion implements Union.ByValue {};
        public KeyInput ki;
        public MouseInput mi;
    }

    //------------------------------
    public static class ByReference extends Win_Input_C implements Structure.ByReference {};

    public static final int INPUT_MOUSE = 0;
    public static final int INPUT_KEYBOARD = 1;
    
    public int type;
    public Win_InputUnion in;

    @Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(
                "type", "in"
        );
    }

}