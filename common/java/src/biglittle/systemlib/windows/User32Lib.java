package biglittle.systemlib.windows;

import com.sun.jna.Library;
import com.sun.jna.Native;

public interface User32Lib extends Library {
    public static User32Lib INSTANCE = (User32Lib) Native.loadLibrary(
            ("User32"), User32Lib.class);

    // Input
    int SendInput(
            int nInputs,
            Win_KeyboardInput_C[] pInputs,
            int cbSize
    );
    int SendInput(
            int nInputs,
            Win_MouseInput_C[] pInputs,
            int cbSize
    );
    int SendInput(
            int nInputs,
            Win_Input_C.ByReference pInputs,
            int cbSize
    );
}