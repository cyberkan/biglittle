package biglittle.systemlib.windows;

import com.sun.jna.Pointer;
import biglittle.ipc.NotifierInf;

public class EventWindows implements NotifierInf {
    private static Kernel32Lib LIB = Kernel32Lib.INSTANCE;
    private Pointer handle;
    boolean manualReset;

    public EventWindows(String name, boolean manualReset) {
        this.manualReset = manualReset;
        handle = LIB.CreateEventA(null, manualReset, false, "Local\\"+name);
    }

    @Override
    public void waitFor() {
        LIB.WaitForSingleObject(handle, LIB.INFINITE);
    }

    @Override
    public void signal() {
        signalEvent();
    }

    @Override
    public void broadcast() {
        signalEvent();
    }

    //---------
    private void signalEvent() {
        if(manualReset) {
            LIB.SetEvent(handle);
            LIB.ResetEvent(handle);
        }
        else
            LIB.SetEvent(handle);
    }
}