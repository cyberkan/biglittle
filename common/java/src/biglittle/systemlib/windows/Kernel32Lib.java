package biglittle.systemlib.windows;

import com.sun.jna.LastErrorException;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class  Kernel32Lib  {
    public static final Kernel32Lib INSTANCE = new Kernel32Lib();
    static {
        Native.register("Kernel32");
    }

    public static final int INFINITE = -1;
    public static final int WAIT_ABANDONED = 0x00000080;
    public static final int WAIT_OBJECT_0 = 0x00000000;
    public static final int WAIT_TIMEOUT = 0x00000102;
    public static final int WAIT_FAILED = -1;

    public static final int MAX_MEM_SIZE_2G = 0x7FFFFFFF;

    public static final int SECTION_QUERY              = 0x0001;
    public static final int SECTION_MAP_WRITE          = 0x0002;
    public static final int SECTION_MAP_READ           = 0x0004;
    public static final int SECTION_MAP_EXECUTE        = 0x0008;
    public static final int SECTION_EXTEND_SIZE        = 0x0010;
    public static final int STANDARD_RIGHTS_REQUIRED   = 0x000F0000;

    public static final int SECTION_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED |
                                                        SECTION_QUERY |
                                                        SECTION_MAP_WRITE |
                                                        SECTION_MAP_READ |
                                                        SECTION_MAP_EXECUTE |
                                                        SECTION_EXTEND_SIZE);

    public static final int FILE_MAP_ALL_ACCESS = SECTION_ALL_ACCESS;
    public static final int PAGE_EXECUTE_READWRITE = 0x04;


    // affinity
    public native boolean GetProcessAffinityMask(
            long hProcess,
            Pointer lpProcessAffinityMask,
            Pointer lpSystemAffinityMask
    );

    public native boolean SetProcessAffinityMask(
            long hProcess,
            long dwProcessAffinityMask
    );

    public native Pointer SetThreadAffinityMask(
            long hThread,
            long dwThreadAffinityMask
    );

    public native long GetCurrentProcess();
    public native long GetCurrentThread();

    // shared memory
    public native Pointer CreateFileMappingA(
            Pointer hFile,
            Pointer pAttributes,
            int flProtect,
            int dwMaximumSizeHigh,
            int dwMaximumSizeLow,
            String lpName
    );


    public native Pointer MapViewOfFile(
            Pointer hFileMappingObject,
            int dwDesiredAccess,
            int dwFileOffsetHigh,
            int dwFileOffsetLow,
            int dwNumberOfBytesToMap) throws LastErrorException;

    // semaphore
    public native Pointer CreateSemaphoreA(
            Pointer lpSemaphoreAttr,
            long lInitialCount,
            long lMaximumCount,
            String lpName );



    public native boolean ReleaseSemaphore(
            Pointer hSemaphore,
            long lReleaseCount,
            Pointer lpPreviousCount);

    // event
    public native Pointer CreateEventA(
            Pointer lpEventAttr,
            boolean bManualReset,
            boolean bInitialState,
            String lpName);

    public native boolean ResetEvent(
            Pointer hEvent);

    public native boolean SetEvent(
            Pointer hEvent);
            
    // wait function
    public native int WaitForSingleObject(
            Pointer hHandle,  // semahore, event, etc
            int dwMilliseconds);
}