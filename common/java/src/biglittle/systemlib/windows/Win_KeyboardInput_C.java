package biglittle.systemlib.windows;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class Win_KeyboardInput_C extends Structure {
    public static final int INPUT_KEYBOARD = 1;
    public static final int KEYEVENTF_EXTENDEDKEY = 0x0001;
    public static final int KEYEVENTF_KEYUP = 0x0002;
    public static final int KEYEVENTF_SCANCODE = 0x0008;
    public static final int KEYEVENTF_UNICODE = 0x0004;
    public static final short VK_MENU = 0x12;

    public int type;
    public int reserved;
    public short wVk;
    public short wScan;
    public int dwFlags;
    public int time;
    public long dwExtraInfo;
    public int reserved1;

    @Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(
                "type", "reserved", "wVk", "wScan", "dwFlags", "time", "dwExtraInfo", "reserved1"
        );
    }
}