package biglittle.systemlib.linux;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class C_cpuset_t extends Structure {
    public long __bits = 0;

    public void set(int cpu) {
        __bits |= (1<< (cpu % 64));
    }

    @Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(
                "__bits"
        );
    }
}