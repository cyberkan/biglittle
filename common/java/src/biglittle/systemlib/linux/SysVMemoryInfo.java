package biglittle.systemlib.linux;

import com.sun.jna.Pointer;

public class SysVMemoryInfo {
    int id;
    long size;
    Pointer mem;
    
    public SysVMemoryInfo(int id, long size, Pointer mem) {
        this.id = id;
        this.size = size;
        this.mem = mem;
    }
}