package biglittle.systemlib.linux;

import com.sun.jna.LastErrorException;
import com.sun.jna.Pointer;
import biglittle.ipc.NamedMemoryInf;

import java.util.HashMap;

public class SharedMemorySysV implements NamedMemoryInf {
    public static final int HEADER_SIZE = 512;
    private static final CLib LIB =  CLib.INSTANCE;
    private static final int SHM_KEY_DOMAIN = 0x01000000;
    private static final int KEY_STRING_MAX_SIZE = 256;

    //////////////////////////////////
    private final String base;
    private final boolean recreate;
    private HashMap<String, SysVMemoryInfo> memoryMap = new HashMap<String, SysVMemoryInfo>();


    private SysVMemoryInfo attachSharedMemorySysV(String keyName) {
        int key = SysVKeyGenerator.getIntHash(SHM_KEY_DOMAIN, keyName);
        C_shm_info shm_info = new C_shm_info();
        C_shmid_ds shm_ds = new C_shmid_ds();
        int shmid = 0;
        int numShm = LIB.shmctl(0, CLib.SHM_INFO, shm_info);

        for (int i = 0; i <= numShm; ++i) {
            try {
                LIB.shmctl(i, CLib.SHM_STAT, shm_ds);
            }
            catch (LastErrorException e) {
                if( e.getErrorCode() == CLib.EACCES || e.getErrorCode() == CLib.EINVAL) // skip if no permission or un-assigned
                    continue;
                throw e;
            }
            if(shm_ds.key == key) {
                shmid = LIB.shmget(key, shm_ds.seg_sz, 0644| CLib.IPC_CREAT);
                break;
            }
        }
        if(shmid <= 0)
            return null;
        System.out.println("("+keyName+"): key = 0x"+Integer.toHexString(key)+", size="+shm_ds.seg_sz);
        Pointer sharedMem = LIB.shmat(shmid, null, 0);
        return new SysVMemoryInfo(shmid, shm_ds.seg_sz, sharedMem);
    }
        static void mlockSharedMemory(SysVMemoryInfo memInfo) {
        try { // prevent paging
            LIB.shmctl(memInfo.id, CLib.SHM_LOCK, (C_shmid_ds)null);
        }
        catch (LastErrorException e) {
            // todo : use logger
            System.out.println("WARN: failed to lock the concurrent segment, shmid="+memInfo.id);
        }
    }

    private long getEffectiveAddr(SysVMemoryInfo memInfo) {
        return Pointer.nativeValue(memInfo.mem) + HEADER_SIZE;
    }

    //
    private boolean checkAndMarkFingerPrint(String keyName, SysVMemoryInfo memInfo) {
        String keyStringfromShm = memInfo.mem.getString(0);
        if(keyStringfromShm.length() == 0) {
            // preload
            LIB.memset(memInfo.mem, 0, memInfo.size);
            memInfo.mem.setString(0, keyName.substring(0));
            return true;
        }
        else if(!keyStringfromShm.equals(keyName)) {
            //System.out.println("Mismatch: KeyString in concurrent concurrent="+keyStringfromShm+", my key="+sharedMem.getString(0));
            throw new LastErrorException(CLib.EEXIST);
        }
        return false;
    }
    private boolean checkFingerPrint(String keyName, SysVMemoryInfo memInfo) {
        String keyStringfromShm = memInfo.mem.getString(0);
        if(keyStringfromShm.length() == 0 || !keyStringfromShm.equals(keyName)) { // not initialized by the creater
            System.out.println("Fingerprint mismatch : expected=["+keyName+"], found=["+keyStringfromShm+"]");
            return false;
        }
        return true;
    }
    private SysVMemoryInfo recreateSharedMemoryIfExists(String keyName, long memorySize) {
        SysVMemoryInfo memInfo = attachSharedMemorySysV(keyName);
        if(memInfo != null) {
            if(!checkAndMarkFingerPrint(keyName, memInfo)) {
                // remove the existing one and recreate
                LIB.shmdt(memInfo.mem);
                LIB.shmctl(memInfo.id, CLib.IPC_RMID, (C_shmid_ds)null);
                memInfo = createSharedMemory(keyName, memorySize);
            }
        }
        else {
            memInfo = createSharedMemory(keyName, memorySize);
        }
        //System.out.println("Attached to "+sharedMem.getString(0));
        return memInfo;
    }

    private SysVMemoryInfo createSharedMemory(String keyName, long size) {
        long memorySize = size + HEADER_SIZE;
        int key = SysVKeyGenerator.getIntHash(SHM_KEY_DOMAIN, keyName);
        System.out.println("["+keyName+"]: key = 0x"+Integer.toHexString(key)+", size="+memorySize);
        int shmid = LIB.shmget(key, memorySize, 0644| CLib.IPC_CREAT);
        Pointer sharedMem = LIB.shmat(shmid, null, 0);
        SysVMemoryInfo memInfo =  new SysVMemoryInfo(shmid, memorySize, sharedMem);
        checkAndMarkFingerPrint(keyName, memInfo);
        return memInfo;
    }
    private SysVMemoryInfo attachSharedMemory(String keyName) {
        SysVMemoryInfo memInfo = attachSharedMemorySysV(keyName);
        if(memInfo != null && checkFingerPrint(keyName, memInfo))
            return memInfo;
        return null;
    }

    public SharedMemorySysV(String base) {
        this(base, false);
    }
    public SharedMemorySysV(String base, boolean recreate) {
        this.base = base;
        this.recreate = recreate;
    }
    
    @Override
    public long create(String name, long size, int flag) {
        String keyName = "Local\\" + base + "/" + name;
        if(keyName.length() > KEY_STRING_MAX_SIZE-1) {
            throw new LastErrorException("The key name is too long:"+keyName);
        }
        SysVMemoryInfo memInfo =  memoryMap.get(name);
        if(memInfo != null) {
            return getEffectiveAddr(memInfo);
        }
        if(recreate) {
            memInfo = recreateSharedMemoryIfExists(keyName, size);
        }
        else {
            memInfo = createSharedMemory(keyName, size);
        }
        mlockSharedMemory(memInfo);

        //System.out.println("Attached to "+sharedMem.getString(0));
        memoryMap.put(name, memInfo);
        return getEffectiveAddr(memInfo);
    }

    @Override
    public long attach(String name) {
        String keyName = "Local\\" + base + "/" + name;
        SysVMemoryInfo memInfo = attachSharedMemory(keyName);
        if(memInfo == null)
            throw new IllegalAccessError("Not able to attach \""+keyName+"\"");
        memoryMap.put(name, memInfo);
        return getEffectiveAddr(memInfo);
    }

}