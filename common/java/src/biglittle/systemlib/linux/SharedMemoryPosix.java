package biglittle.systemlib.linux;


import biglittle.ipc.NamedMemoryInf;

public class SharedMemoryPosix implements NamedMemoryInf {
    private static CLib LIB = CLib.INSTANCE;
    private static PThreadLib PLIB = PThreadLib.INSTANCE;

    @Override
    public long create(String name, long size, int flag) {
        return 0;
    }

    @Override
    public long attach(String name) {
        return 0;
    }
}
