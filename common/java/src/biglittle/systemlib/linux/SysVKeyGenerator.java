package biglittle.systemlib.linux;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SysVKeyGenerator {
    public static int getIntHash(int domain,String strKey) {
        MessageDigest md;
        byte[] d;
        try {
            md = MessageDigest.getInstance("MD5");
            d = md.digest(strKey.getBytes());
        } catch (NoSuchAlgorithmException e)
        {
            d = strKey.getBytes();
        }

        int hash = 1;
        for (byte aD : d) {
            hash *= aD | 1;
            hash += aD;
        }
        hash |= domain;
        return hash;
    }
}