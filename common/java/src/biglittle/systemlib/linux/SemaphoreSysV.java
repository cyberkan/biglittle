package biglittle.systemlib.linux;

import com.sun.jna.LastErrorException;
import biglittle.ipc.NotifierInf;
import biglittle.ipc.SemaphoreInf;

public class SemaphoreSysV implements SemaphoreInf, NotifierInf {
    private static final CLib LIB =  CLib.INSTANCE;
    private static final C_sembuf acq_buf = new C_sembuf((short)0,(short)-1,(short)0);
    private static final C_sembuf rel_buf = new C_sembuf((short)0,(short)1,(short)0);
    private static final C_sembuf wait_0_buf = new C_sembuf((short)0,(short)0,(short)0);
    private static final int SEM_KEY_DOMAIN = 0xF0000000;
    private int semid;

    public SemaphoreSysV(String name) {
        this(SysVKeyGenerator.getIntHash(SEM_KEY_DOMAIN, name), 1);
    }
    public SemaphoreSysV(String name, int maxCnt) {
        this(SysVKeyGenerator.getIntHash(SEM_KEY_DOMAIN, name), maxCnt);
    }

    public SemaphoreSysV(int key, int maxCnt) {
        try {
            semid = LIB.semget(key, 1, 0644| CLib.IPC_CREAT | CLib.IPC_EXCL);
        }
        catch (LastErrorException e) {
            semid = LIB.semget(key, 1, 0644| CLib.IPC_CREAT);
            LIB.semctl(semid, 0, CLib.SETVAL, maxCnt);
        }
    }
   
    @Override
    public void acquire() {
        LIB.semop(semid, acq_buf, 1);
    }
    
    @Override
    public void release() {
        LIB.semop(semid, rel_buf, 1);
    }

    @Override
    public void waitFor() {
        LIB.semop(semid, wait_0_buf, 1);
        LIB.semop(semid, rel_buf, 1);
    }
    
    @Override
    public void signal() {
        LIB.semctl(semid, 0, CLib.SETVAL, 0);
    }
    
    @Override
    public void broadcast() {
        LIB.semctl(semid, 0, CLib.SETVAL, 0);
    }
}