package biglittle.systemlib.linux;

import com.sun.jna.*;

public class CLib {
    public static final CLib INSTANCE = new CLib();
    
    static {
        Native.register("c");
    }

// oflags
    public static final int O_RDONLY   = 00;
    public static final int O_WRONLY   = 01;
    public static final int O_RDWR     = 02;
    public static final int O_CREAT    = 0100;
    public static final int O_EXCL     = 0200;
    public static final int O_TRUNC    = 01000;
    public static final int O_APPEND   = 02000;
    public static final int O_NONBLOCK = 04000;

// memory access
    public static final int PROT_NONE  = 0x0;
    public static final int PROT_READ  = 0x1;
    public static final int PROT_WRITE = 0x2;
    public static final int PROT_EXEC  = 0x4;
    public static final int PROT_SEM   = 0x8;
    
    public static final int MAP_SHARED    = 0x01;
    public static final int MAP_PRIVATE   = 0x02;
    public static final int MAP_FIXED     = 0x10;
    public static final int MAP_ANONYMOUS = 0x20;
    public static final int MAP_ANON      = MAP_ANONYMOUS;
    
    public static final int MAP_GROWSDOWN  = 0x00100;
    public static final int MAP_EXECUTABLE = 0x01000;  
    public static final int MAP_LOCKED     = 0x02000;
    public static final int MAP_NORESERVE  = 0x04000;
    public static final int MAP_POPULATE   = 0x08000;
    public static final int MAP_NONBLOCK   = 0x10000;
    public static final int MAP_HUGETLB    = 0x40000;
    
// flags for msync
    public static final int MS_ASYNC   = 1;
    public static final int MS_SYNC    = 4;
    
// SysV flags        
    public static final int IPC_CREAT  = 00001000;
    public static final int IPC_EXCL   = 00002000;
    public static final int IPC_RMID   = 0;
    public static final int IPC_STAT   = 1;
    public static final int SHM_LOCK   = 11;
    public static final int SHM_STAT   = 13;
    public static final int SHM_INFO   = 14;
    public static final int SETVAL     = 16;
    public static final int SEM_UNDO   = 4096;

// futex
    public static final int FUTEX_WAIT = 0;
    public static final int FUTEX_WAKE = 1;
    // values that may different based on data model
    public static final int SYS_futex = (System.getProperty("sun.arch.data.model").equalsIgnoreCase("64")) ? 202 : 240;

// eventfd
    public static final int EFD_SEMAPHORE = 1;
    public static final int EFD_NONBLOCK  = 04000;
    public static final int EFD_CLOEXEC   = 02000000;

// error codes
    public static final int EPERM      = 1;   // operation is not permitted
    public static final int ENOENT     = 2;   // no such file, directory, segment or resource
    public static final int EINTR      = 4;   // Interrupted system call
    public static final int EIO        = 5;   // I/O error
    public static final int E2BIG      = 7;   // argument list too long
    public static final int EBADF      = 9;   // bad file descriptor
    public static final int EAGAIN     = 11;  // try again
    public static final int ENOMEM     = 12;  // out of memory
    public static final int EACCES     = 13;  // permission denied
    public static final int EFAULT     = 14;  // bad address
    public static final int EEXIST     = 17;  // file already exists
    public static final int ENODEV     = 19;  // not able to mount inode device
    public static final int EISDIR     = 21;  // file descriptor refers to a directory
    public static final int EINVAL     = 22;  // invalid argument
    public static final int ENFILE     = 23;  // system-wide max open files reached
    public static final int EMFILE     = 24;  // per-process max open file descriptors reached
    public static final int EFBIG      = 27;  // file too large
    public static final int ENOSPC     = 28;  // no space left on device
    public static final int EPIPE      = 32;  // broken pipe
    public static final int ERANGE     = 34;  // out of range
    public static final int ENOSYS     = 38;  // invalid operation, not implemented
    public static final int EIDRM      = 43;  // identifier removed
    public static final int EOVERFLOW  = 75;  // value is too large

    public native int shmget(final int key, final long size, final int flag) throws LastErrorException;
    public native Pointer shmat(final int shmid, Pointer shmaddr, final int flag) throws LastErrorException;
    public native int shmdt(final Pointer shmaddr) throws LastErrorException;
    //int shmctl(final int shmid, final int cmd, Pointer buf) throws LastErrorException;
    public native int shmctl(final int shmid, final int cmd, C_shmid_ds buf) throws LastErrorException;
    public native int shmctl(final int shmid, final int cmd, C_shm_info buf) throws LastErrorException;
    public native int mlock(final Pointer addr, long len) throws LastErrorException;
    public native Pointer memset(Pointer addr, final int c, long len) throws LastErrorException;
    public native int semget(final int key, final int nsems, final int semflag) throws LastErrorException;
    public native int semop(final int semid, C_sembuf sembuf, final int nsops) throws LastErrorException;
    public native int semctl(final int semid, final int semnum, int cmd, int val) throws LastErrorException;
    //int syscall(int number, Pointer futex, int op, int val, Pointer t, Pointer futex2, int val2);
    public native int syscall(int number, long pfutex, int op, int val, long ptimespec, long pfutex2, int val2);
    // use only if Linux kernel is equal to or higher than 2.6.27
    public native int eventfd(int initval, int flags);
    public native long mmap(long addr, long length, int prot, int flags, int fd, long offset);
}