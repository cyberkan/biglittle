package biglittle.systemlib.linux;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

// make public class and public for all data members
public class C_shm_info extends Structure {
    public int used_ids;
    public long shm_tot;
    public long shm_res;
    public long shm_swp;
    public long swap_attempts;
    public long swap_successes;

    @Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(
                "used_ids", "shm_tot", "shm_res", "shm_swp", "swap_attempts", "swap_successes"
        );
    }
}