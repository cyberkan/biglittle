package biglittle.systemlib.linux;

import com.sun.jna.Library;
import com.sun.jna.Native;

public interface PThreadLib extends Library {
    static final PThreadLib INSTANCE = (PThreadLib) Native.loadLibrary(
            "pthread", PThreadLib.class);

    long pthread_self();
    int pthread_setaffinity_np(long pthread_handle, long size, C_cpuset_t cpuset);
    int shm_open(String name, int oflag, int mode);
    int shm_unlink(String name);
}