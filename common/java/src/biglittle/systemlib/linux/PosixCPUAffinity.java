package biglittle.systemlib.linux;

import biglittle.systemlib.CPUAffinity;

public class PosixCPUAffinity implements CPUAffinity {
    private C_cpuset_t cpuset = new C_cpuset_t();
    private volatile long nativeHandle = 0;

    public PosixCPUAffinity() {
    }
    public PosixCPUAffinity(long handle) {
        nativeHandle = handle;
    }

    @Override
    public void setThreadHandle() {
        nativeHandle = PThreadLib.INSTANCE.pthread_self();
    }

    @Override
    public void setTask() {
        if(nativeHandle != 0)
            PThreadLib.INSTANCE.pthread_setaffinity_np(nativeHandle, cpuset.size(), cpuset);
    }

    @Override
    public void setAffinity(int cpu) {
        cpuset.set(cpu);
        setTask();
    }
}