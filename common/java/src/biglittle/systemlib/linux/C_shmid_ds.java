package biglittle.systemlib.linux;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class C_shmid_ds extends Structure {
    public int key;              
    public int uid;              
    public int gid;              
    public int cuid;             
    public int cgid;             
    public short mode;           
    public short pad1;           
    public short seq;            
    public short pad2;           
    public int __unused0;
    public long __unused1;
    public long __unused2;

    public long seg_sz;
    public long atime;
    public long dtime;
    public long ctime;
    public int cpid;
    public int lpid;
    public long nattach;
    public long __unused4;
    public long __unused5;

    @Override
    protected List<String> getFieldOrder() {
        return Arrays.asList(
                "key", "uid", "gid", "cuid", "cgid", "mode", "pad1", "seq", "pad2", "__unused0", "__unused1", "__unused2",
                "seg_sz", "atime", "dtime", "ctime", "cpid", "lpid", "nattach", "__unused4", "__unused5"
        );
    }
}