package biglittle.systemlib.linux;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class C_sembuf extends Structure {
    public short sem_num;
    public short sem_op;
    public short sem_flag;

    public C_sembuf(short sem_num, short sem_op, short sem_flag) {
        this.sem_num = sem_num;
        this.sem_op = sem_op;
        this.sem_flag = sem_flag;
    }
    @Override
    protected List getFieldOrder() {
        return Arrays.asList(new String[]{
                "sem_num", "sem_op", "sem_flag"
        });
    }
}