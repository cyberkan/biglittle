package biglittle.systemlib;

import biglittle.ipc.NamedMemoryInf;
import biglittle.ipc.NotifierInf;
import biglittle.ipc.SemaphoreInf;
//import systemlib.linux.FutexLinux;
import biglittle.systemlib.linux.PosixCPUAffinity;
import biglittle.systemlib.linux.SemaphoreSysV;
import biglittle.systemlib.linux.SharedMemorySysV;
import biglittle.systemlib.windows.EventWindows;
import biglittle.systemlib.windows.SemaphoreWindows;
import biglittle.systemlib.windows.SharedMemoryWindowsMmap;
import biglittle.systemlib.windows.WindowsCPUAffinity;

public class NativeResource {

    public static final int OS_LINUX = 1;
    public static final int OS_UNIX = 100;
    public static final int OS_WINDOWS = 101;
    public static final int OS_WINDOWS7 = 107;
    public static final int OS_WINDOWS8 = 108;
    public static final int OS_WINDOWS10 = 110;
    public static int OS;

    static {
        String os = System.getProperty("os.name");
        //System.out.println("OS="+os);
        if (os.startsWith("Windows")) {
            if(os.equalsIgnoreCase("windows 7"))
                OS = OS_WINDOWS7;
            else if(os.equalsIgnoreCase("windows 8"))
                OS = OS_WINDOWS8;
            else if(os.equalsIgnoreCase("windows 10"))
                OS = OS_WINDOWS10;
            else
                OS = OS_WINDOWS;
        }
        else if (os.startsWith("Linux")) {
            OS = OS_LINUX;
        }
        else
            OS = OS_UNIX;
    }

    public static CPUAffinity getCPUAffinity() {
        switch(OS) {
            case OS_LINUX:
            case OS_UNIX : return new PosixCPUAffinity();
            case OS_WINDOWS7:
            case OS_WINDOWS8:
            case OS_WINDOWS10:
            case OS_WINDOWS: return new WindowsCPUAffinity();
            default:  return new WindowsCPUAffinity();
        }
    }
    public static NamedMemoryInf getNamedSharedMemory(String name, boolean recreat) {
        switch(OS) {
            case OS_LINUX:
            case OS_UNIX : return new SharedMemorySysV(name, recreat);
            case OS_WINDOWS7:
            case OS_WINDOWS8:
            case OS_WINDOWS10:
            case OS_WINDOWS: return new SharedMemoryWindowsMmap(name);
            default:  return new SharedMemorySysV(name, recreat);
        }
    }
    public static SemaphoreInf getNamedSemaphore(String name) {
        switch(OS) {
            case OS_LINUX:
            case OS_UNIX : return new SemaphoreSysV(name);
            case OS_WINDOWS7:
            case OS_WINDOWS8:
            case OS_WINDOWS10:
            case OS_WINDOWS: return new SemaphoreWindows(name);
            default:  return new SemaphoreSysV(name);
        }
    }
    public static NotifierInf getNamedNotifier(String name) {
        switch(OS) {
            case OS_LINUX: //return new FutexLinux(name);
            case OS_UNIX : return new SemaphoreSysV(name);
            case OS_WINDOWS7:
            case OS_WINDOWS8:
            case OS_WINDOWS10:
            case OS_WINDOWS: return new EventWindows(name, true);
            default:  return new SemaphoreSysV(name);
        }
    }
}