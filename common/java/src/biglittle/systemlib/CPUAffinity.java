package biglittle.systemlib;

public interface CPUAffinity {
    public void setThreadHandle();
    public void setTask();
    public void setAffinity(int cpu);
}