package biglittle.ipc;

public interface NamedMemoryInf {
    long create(String name, long size, int flag); // if already exist, attach it
    long attach(String name);
}