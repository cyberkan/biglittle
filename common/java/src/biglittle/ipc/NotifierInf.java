package biglittle.ipc;

public interface NotifierInf {
    public void waitFor();
    public void signal();
    public void broadcast();
}