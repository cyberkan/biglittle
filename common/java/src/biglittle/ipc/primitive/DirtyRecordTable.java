package biglittle.ipc.primitive;

import biglittle.systemlib.Utility;

public class DirtyRecordTable extends NativeMemory {
    private static final long PAGE_OFFSET = 0;
    private static final long LOCK_OFFSET = 8;
    private static final long COUNT0_OFFSET = 16;
    private static final long COUNT1_OFFSET = 24;
    private static final long HEADER_SIZE = 64;

    private long size;
    private long[] TABLE_OFFSET = new long[2];
    private long[] COUNT_OFFSET = new long[] {COUNT0_OFFSET, COUNT1_OFFSET};
    private long EV_TBL_SIZE;
    private long ONE_TABLE_SIZE;

    public DirtyRecordTable(long size) {
        this.size = size;
        EV_TBL_SIZE = size * 8;
        ONE_TABLE_SIZE = Utility.roundTo64(8 + EV_TBL_SIZE + size);
        TABLE_OFFSET[0] = HEADER_SIZE;
        TABLE_OFFSET[1] = HEADER_SIZE + ONE_TABLE_SIZE;
        base = NativeMemory.UNSAFE.allocateMemory(HEADER_SIZE + ONE_TABLE_SIZE * 2);
        NativeMemory.UNSAFE.setMemory(base, HEADER_SIZE + ONE_TABLE_SIZE * 2, (byte) 0);
        this.page = 1;
    }

    //---- for writer
    public void markDirty(long loc) {
        if(loc >= size)
            throw new IllegalArgumentException("Record="+loc+" is larger than table capacity="+size);

        markProgress();
        long wpage = getVolatileLong(PAGE_OFFSET);
        long offset =  TABLE_OFFSET[(int)wpage];
        long loc_dirtymark = 8+ EV_TBL_SIZE + loc;
        boolean isClean = NativeMemory.UNSAFE.getByteVolatile(null, base+offset+loc_dirtymark) == 0;
        if(isClean) {
            //System.out.println("marking dirty for "+loc);
            NativeMemory.UNSAFE.putByteVolatile(null, base + offset + loc_dirtymark, (byte) 1);
            long cnt_offset = COUNT_OFFSET[(int)wpage];
            long cnt = getVolatileLong(cnt_offset);
            putOrderedLong(offset+8+cnt*8, loc);
            putOrderedLong(cnt_offset, cnt+1);
        }
        completeProgress();
    }

    //---- for reader
    private long readCnt = 0;
    private long page = 1;
    private long dirtyCnt = 0;

    public void flip() {
        // if no data from writer, no need to flip
        long newPage = page ^ 0x01L;
        if(getVolatileLong(COUNT_OFFSET[(int)newPage]) == 0)
            return;
        long offset =  TABLE_OFFSET[(int)page];
        NativeMemory.UNSAFE.setMemory(base+offset, ONE_TABLE_SIZE, (byte)0);
        putOrderedLong(COUNT_OFFSET[(int)page], 0);
        putOrderedLong(PAGE_OFFSET, page);
        waitForComplete();
        readCnt = 0;
        this.page = newPage;
        this.dirtyCnt = getVolatileLong(COUNT_OFFSET[(int)page]);
        //if(dirtyCnt > 2)
        //System.out.println("readCnt="+readCnt+", dirtyCnt="+dirtyCnt+", page="+page);
    }

    public long getTotalNumberOfDirtyRecords() {
        return getVolatileLong(COUNT_OFFSET[(int)page]);
    }
    public long getDirtyRecord() {
        if(readCnt >= dirtyCnt)
            return -1;
        long offset =  TABLE_OFFSET[(int)page];
        long loc =  getVolatileLong(offset+8+(readCnt)*8);
        //System.out.println("Reading a record at "+readCnt+" = "+loc);
        ++readCnt;
        return loc;
    }

    //------------
    private void markProgress() {
        putOrderedLong(LOCK_OFFSET, 1);
    }

    private void completeProgress() {
        putOrderedLong(LOCK_OFFSET, 0);
    }

    private void waitForComplete() {
        long cnt = 0;
        while(getVolatileLong(LOCK_OFFSET) != 0)
            Utility.busyDelay((int)++cnt);

    }
}
