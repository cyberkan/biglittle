package biglittle.ipc.primitive;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

public class NativeMemory {
    public static Unsafe UNSAFE;
    static {
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            UNSAFE = (Unsafe) field.get(null);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }
    }
    private static final long DIRTY_MARK_MASK = 0x00000000FFFFFFFFL;
    
    public static void msleep(long msec) {
        nanosleep(msec * 1000000);
    }
    public static void usleep(long usec) {
        nanosleep(usec * 1000);
    }
    public static void nanosleep(long nsec) {
        if(nsec > 0)
            UNSAFE.park(false, nsec);
    }
    public static long getLong(long base, long offset) {
        return UNSAFE.getLong(0, base+offset);
    }
    public static long getVolatileLong(long base, long offset) {
        return UNSAFE.getLongVolatile(null, base+offset);
    }
    public static void putOrderedLong(long base, long offset, long value) {
        UNSAFE.putOrderedLong(null, base+offset, value);
    }
    public static void putVolatileLong(long base, long offset, long value) {
        UNSAFE.putLongVolatile(null, base+offset, value);
    }
    public static double getVolatileDouble(long base, long offset) {
        return UNSAFE.getDoubleVolatile(null, base+offset);
    }
    public static void putVolatileDouble(long base, long offset, double value) {
        UNSAFE.putDoubleVolatile(null, base+offset, value);
    }

    //------------------------------------
    protected long base = 0;

    public NativeMemory() {}
    public NativeMemory(long base) {
        this.base = base;
    }
    public long getBase() {
        return base;
    }
    public void rebase(long addr) {
        base = addr;
    }
    public long getLong(long offset) {
        return UNSAFE.getLong(null, base+offset);
    }
    // getVolatile* uses "acquire" semantic. in x86 no extra ops involved.
    public long getVolatileLong(long offset) {
        return UNSAFE.getLongVolatile(null, base + offset);
    }

    // putVolatile* uses "seq-cst" semantic. In x86, it uses lock prefix (or equivalent xchg)
    //                                      - bc lock/xchg is known as cheaper than mfence
    // putOrdered* uses "release" semantic. In x86 no extra ops involved.
    public void putOrderedLong(long offset, long value) {
        UNSAFE.putOrderedLong(null, base+offset, value);
    }

    public void putVolatileLong(long offset, long value) {
        UNSAFE.putLongVolatile(null, base+offset, value);
    }

    public double getVolatileDouble(long offset) {
        return UNSAFE.getDoubleVolatile(null, base + offset);
    }

    public void putVolatileDouble(long offset, double value) {
        UNSAFE.putDoubleVolatile(null, base + offset, value);
    }

    public long fetchAndAddLong(long offset, long val) {
        long x;
        do {
            x = getVolatileLong(offset);
        } while(!compareAndSwapLong(offset, x, x+val));
        return x;
    }

    public long fetchAndAddLong(long offset, long val, long min, long max) {
        long x, y;
        do {
            x = getVolatileLong(offset);
            y = x+val;
            y = (y >= max) ? y - max + min : y;
        } while(!compareAndSwapLong(offset, x, y));
        return x;
    }

    public long fetchAndLong(long offset, long val) {
        long x;
        do {
            x = getVolatileLong(offset);
        } while(!compareAndSwapLong(offset, x, x & val));
        return x;
    }

    public long fetchOrLong(long offset, long val) {
        long x;
        do {
            x = getVolatileLong(offset);
        } while(!compareAndSwapLong(offset, x, x | val));
        return x;
    }

    public boolean compareAndSwapLong(long offset, long expect, long value) {
        return UNSAFE.compareAndSwapLong(null, base+offset,expect,value);
    }

    public void acquire(long offset) {
        long marker = getVolatileLong(offset);
        long lowMarker = marker & DIRTY_MARK_MASK;
        ++lowMarker;
        putOrderedLong(offset, (marker & ~DIRTY_MARK_MASK)|(lowMarker & DIRTY_MARK_MASK ));
    }

    public void release(long offset) {
        long marker = getVolatileLong(offset);
        putOrderedLong(offset, (marker << 32)|(marker & DIRTY_MARK_MASK));
    }

    public void memcopy(long src, long dest, long size) {
        UNSAFE.copyMemory(src, dest, size);
    }
    public boolean isReleased(long marker) {
        return ((marker >> 32) & DIRTY_MARK_MASK) == (marker & DIRTY_MARK_MASK);
    }

}