package biglittle.ipc.primitive;

import biglittle.ipc.StructureInArray;

public class SharedCache<T extends StructureInArray> {
    private static final long RECSZ_OFFSET = 0;
    private static final long NUMREC_OFFSET = 8;
    private static final long HEADER_SIZE = 64;

    public static long getMemorySizeFor(long recSize, long numRec) {
        return HEADER_SIZE + recSize * numRec;
    }

    //----------------
    private long numRecs;
    private long recSize;

    private long base;

    public SharedCache(long base, long recSize, long numRecs) {
        this.base = base;
        this.recSize = recSize;
        this.numRecs = numRecs;
        NativeMemory.UNSAFE.putLong(null, base + NUMREC_OFFSET, numRecs);
        NativeMemory.UNSAFE.putOrderedLong(null, base+RECSZ_OFFSET, recSize);
    }

    public SharedCache(long base) {
        this.base = base;
        do {
            this.recSize = NativeMemory.UNSAFE.getLongVolatile(null, base + RECSZ_OFFSET);
        } while (this.recSize == 0);
        this.numRecs = NativeMemory.UNSAFE.getLong(null, base + NUMREC_OFFSET);
    }

    public long capacity() {
        return numRecs;
    }

    public void seek(T rec, long index) {
        if(index >= numRecs || index < 0)
            throw new IllegalArgumentException("Index is too high or negative: "+index);
        rec.rebase(base + HEADER_SIZE + recSize * index);
    }
}