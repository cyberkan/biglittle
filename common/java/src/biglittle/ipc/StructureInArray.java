package biglittle.ipc;

public interface StructureInArray {
    public void rebase(long address);
    public long size();
}