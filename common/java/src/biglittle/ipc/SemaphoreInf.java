package biglittle.ipc;

public interface SemaphoreInf {
    public void acquire();
    public void release();
}