///////////////////////////////
// rcs_id = "$Id$"
///////////////////////////////

#ifndef __LOCKFREEQUEUE2_H__
#define __LOCKFREEQUEUE2_H__

#include <atomic>
#include <mutex>

#include <TemplateUtil.h>

//#define VOLATILE_FENCE volatile
#define LFENCE std::atomic_thread_fence(std::memory_order_acquire)
#define SFENCE std::atomic_thread_fence(std::memory_order_release)

template<typename T, uint64_t N, uint8_t M>
class LockFreeQueue2 {
public:
    
    enum : uint64_t {
        capacity = CeilToPow2<N>::value,
        MASK = capacity -1
    };

private:
    struct Index 
    {
        uint64_t val;
        //uint64_t next;
        //uint8_t padding[56];
    };
    std::mutex gen_reader_mutex __attribute__((aligned(64)));
    Index head_ ;
    decltype(head_.val) hnext;
    uint64_t marker_mask;
    std::atomic<uint32_t> reader_cnt;
    struct UserRecord
    {
        std::atomic<uint64_t> marker;
        T d;
        UserRecord() : marker(0), d{} {}
    } __attribute__((aligned(64)));
    UserRecord data[capacity];

    auto head() -> decltype(head_.val) {return head_.val;}
    auto head_ref() -> volatile decltype(head_.val)& {return head_.val;}
    
    auto next_head() -> decltype(head_.val) { return ((head_.val+1) & MASK);}
    auto inchead() ->decltype(head_.val) { return head_.val = hnext;}
    
public:

    //--------------------------------
    
    class Reader
    {
        
        //volatile Index & head_;
        uint64_t reader_mask;
        Index tail_;
        decltype(tail_.val) tnext;
        UserRecord * data;
        uint16_t id;
        
        //auto head() -> decltype(head_.val) {return head_.val;}
        auto tail() -> decltype(tail_.val) {return tail_.val;}
        auto next_tail() -> decltype(tail_.val) { return ((tail_.val+1) & MASK);}
        auto inctail() -> decltype(tail_.val) { return tail_.val = tnext;}
        void clear_marker() { data[tail()].marker.fetch_and(~reader_mask, std::memory_order_release); }
    public: 
        Reader(uint16_t i, UserRecord* d) 
            : reader_mask(1<<i), tail_{0}, data(d), id(i) {}
        
        const T& peek() {
            tnext = next_tail();
            uint64_t wait_cnt = 0;
            bool stuck_in_loop = true;
            while ((data[tnext].marker.load(std::memory_order_acquire) & reader_mask) == 0)
                if(++wait_cnt > 10000 && !stuck_in_loop) {
                    std::cout << "C"<<id<<"[t"<<tail()<<"-n"<<tnext<< "]" << data[tnext].marker << std::endl;
                    stuck_in_loop = true;
                }
            //while (LFENCE, tail()==head());

            //std::cout << "-";
            //auto next = next_tail();
            //if(next == 0) std::cout <<"#";
            return data[tnext].d;
        }
    
        void finish() {
            inctail();
            //SFENCE;
            clear_marker();
        }
    };

    LockFreeQueue2() : head_{0}, data{} { std::cout<<"capacity="<<capacity<<", mask="<<std::hex<<MASK<<std::dec<<std::endl;};
    // auto issue has been fixed in 4.9.0

    Reader genReader()
    {
        std::lock_guard<std::mutex> lock(gen_reader_mutex);
        uint32_t cnt = reader_cnt.fetch_add(1);
        marker_mask |= (1<<cnt);
        std::cout<<"creating reader-"<<cnt<<", mask="<<marker_mask<<std::endl;
        return Reader(cnt, this->data);
        //return r;
    }

    T& reserve() {
        hnext = next_head();
        uint64_t wait_cnt = 0;
        bool stuck_in_loop = true;
        while(data[hnext].marker.load(std::memory_order_acquire))
            if(++wait_cnt>10000 && !stuck_in_loop) {
                std::cout<<"P[h"<<head()<<"-n"<<hnext<<"]"<<data[hnext].marker<<std::endl;
                stuck_in_loop = true;
            }
              //next == tail_[0].val || next == tail_[1].val );
              //next == tail_[0].val );
        //if(next ==0) std::cout<<"*";
        return data[hnext].d;
    }

    void poke() {
        head_ref()=hnext;
        SFENCE;
        //asm volatile ("" : : : "memory");
        data[head()].marker.store(marker_mask, std::memory_order_relaxed);
        //if(next == 0) std::cout<<"head="<<next<<",marker="<<data[next].marker<<std::endl;
                
    }

};

#endif //__LOCKFREEQUEUE2_H__