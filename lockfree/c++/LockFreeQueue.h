///////////////////////////////
// rcs_id = "$Id$"
///////////////////////////////

#ifndef __LOCKFREEQUEUE_H__
#define __LOCKFREEQUEUE_H__

#include <TemplateUtil.h>

#define LFENCE std::atomic_thread_fence(std::memory_order_acquire)
#define SFENCE std::atomic_thread_fence(std::memory_order_release)

template <typename DERIVED, uint64_t N>
struct AbstractLockFreeQueue
{
    enum : uint64_t {
        capacity = CeilToPow2<N>::value,
        MASK = capacity -1
    };

    //using DERIVED::genReader;
};

template<typename T, uint64_t N, uint8_t M>
class LockFreeQueue : public AbstractLockFreeQueue<LockFreeQueue<T,N,M>, N>
{
public:
    using BASE = AbstractLockFreeQueue<LockFreeQueue<T,N,M>, N>;
    using BASE::capacity;
    using BASE::MASK;

private:
    typedef T T64 __attribute__((aligned(64)));
    struct Index 
    {
        uint64_t val;
        //uint8_t padding[56];
    };
    Index head_;
    Index tail_[M];
    std::atomic<uint32_t> reader_cnt;
    T64 data[BASE::capacity];

    auto head() -> decltype(head_.val) {return head_.val;}
    auto head_ref() -> decltype(head_.val)& {return head_.val;}
    auto tail() -> decltype(tail_[0].val) {return tail_[0].val;}
    auto tail_ref() -> decltype(tail_[0].val)& {return tail_[0].val;}
    
    auto next_head() -> decltype(head_.val) { return (head_.val+1) & MASK;}
    auto inchead() ->decltype(head_.val) { return head_.val = next_head();}
    auto next_tail() -> decltype(tail_[0].val) { return (tail_[0].val+1) & MASK;}
    auto inctail() -> decltype(tail_[0].val) { return tail_[0].val = next_tail();}
    inline bool is_occupied_slot(uint32_t next) { 
        //for(uint32_t n = reader_cnt.load(std::memory_order_acquire), i = 0; i < n; ++i)
        //    if(next == tail_[i].val) return true;
        switch(reader_cnt.load(std::memory_order_acquire)) {
            case 1: return (next == tail_[0].val);
            case 2: return (next == tail_[0].val || next == tail_[1].val);
            case 3: return (next == tail_[0].val || next == tail_[1].val || next == tail_[2].val);
            case 4: return (next == tail_[0].val || next == tail_[1].val || next == tail_[2].val || next == tail_[3].val);    
        }
        return false;
    }
    
public:

    //--------------------------------
    
    class Reader
    {
        Index & head_;
        Index & tail_;
        T * data;
        
        auto head() -> decltype(head_.val) {return head_.val;}
        auto tail() -> decltype(tail_.val) {return tail_.val;}
        auto next_tail() -> decltype(tail_.val) { return (tail_.val+1) & MASK;}
        auto inctail() -> decltype(tail_.val) { 
            return tail_.val = next_tail();
        }
        
    public: 
        Reader(Index& h, Index& t, T* d) 
            : head_(h), tail_(t), data(d) {}
        const T& peek() {
            while (LFENCE, tail()==head());
            //std::cout << "-";
            auto next = next_tail();
            return data[next];
        }
    
        void finish() {
            SFENCE;
            inctail();        
        }
        
    };

    LockFreeQueue() : head_{}, tail_{} { std::cout<<"capacity="<<capacity<<", MASK="<<std::hex<<MASK<<std::dec<<std::endl;};
    // auto issue has been fixed in 4.9.0

    Reader genReader()
    {
        uint32_t cnt = reader_cnt.fetch_add(1);
        Reader r(head_, tail_[cnt], data);
        return r;
    }

    T& reserve() {
        auto next = next_head();
        while(LFENCE,
              is_occupied_slot(next)); // too slow
              //next == tail_[0].val || next == tail_[1].val );
              //next == tail_[0].val );

        return data[next];
    }

    void poke() {
        SFENCE;
        head_ref()=next_head();        
    }

    /*
    bool poke(T&& d, bool wait_in_loop) {
        auto next = next_head();
        //std::atomic_thread_fence(std::memory_order_acquire);
        while(
              LFENCE,
              //std::atomic_thread_fence(std::memory_order_seq_cst),
              next==tail()) {
            if(!wait_in_loop)
                return false;
                
        }
        data[next] = d;
        SFENCE;
        //std::atomic_thread_fence(std::memory_order_seq_cst);
        head_ref()=next;
        return true;
    }

    
    const T& peek() {
        while (LFENCE,
               tail()==head());
        
        auto next = next_tail();
        return data[next];
    }
    
    void finish() {
        SFENCE;
        //std::atomic_thread_fence(std::memory_order_seq_cst);
        tail_ref() = next_tail();        
    }
    
    bool peek(const T& d) {
        LFENCE;
        //std::atomic_thread_fence(std::memory_order_seq_cst);
        if (tail()==head()) 
            return false;
        
        auto next = next_tail();
        //auto& dref = data[next];
        //if(dref!=1) {
        //    std::cout<<"reordered found in consumer - "<< tail() << std::endl;
        //    return false;
        //}
        d = data[next];
        SFENCE;
        //std::atomic_thread_fence(std::memory_order_seq_cst);
        tail_ref() = next;
        return true;
    }
    */
};

#endif //__LOCKFREEQUEUE_H__
