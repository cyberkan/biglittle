///////////////////////////////
// rcs_id = "$Id$"
///////////////////////////////

#ifndef __LOCKFREEQUEUE3_H__
#define __LOCKFREEQUEUE3_H__

#include <atomic>
#include <mutex>

#include <TemplateUtil.h>

//#define VOLATILE_FENCE volatile
#define LFENCE std::atomic_thread_fence(std::memory_order_acquire)
#define SFENCE std::atomic_thread_fence(std::memory_order_release)

template<typename T, uint64_t N, uint8_t M>
class LockFreeQueue3 {
    struct Index 
    {
        uint64_t val;
        uint64_t next;
        //uint8_t padding[56];
    };
    Index head_;
    std::atomic<uint32_t> reader_cnt;

public:
    
    enum : uint64_t {
        capacity = CeilToPow2<N>::value,
        MASK = capacity -1
    };

private:

    struct UserRecord
    {
        union {
            uint8_t marker[64];
            uint64_t marker64[8];
        } m;
        T d;
        UserRecord() : m{}, d{} {}
    };
    UserRecord data[capacity];
    std::mutex gen_reader_mutex;

    auto head() -> decltype(head_.val) {return head_.val;}
    auto head_ref() -> volatile decltype(head_.val)& {return head_.val;}
    
    auto next_head() -> decltype(head_.val) { return (head_.next = (head_.val+1) & MASK);}
    auto inchead() ->decltype(head_.val) { return head_.val = next_head();}
    
public:

    //--------------------------------
    
    class Reader
    {
        
        Index tail_;
        UserRecord * data;
        uint16_t id;
        //char pad[62];
        
        //auto head() -> decltype(head_.val) {return head_.val;}
        auto tail() -> decltype(tail_.val) {return tail_.val;}
        auto next_tail() -> decltype(tail_.val) { return (tail_.next = (tail_.val+1) & MASK);}
        auto inctail() -> decltype(tail_.val) { return tail_.val = tail_.next;}
        void clear_marker() { data[tail()].m.marker[id] = 0; }
    public: 
        Reader(uint16_t i, UserRecord* d) 
            : tail_{0,1}, data(d), id(i) {}
        
        const T& peek() {
            auto next = next_tail();
            uint64_t wait_cnt = 0;
            bool stuck_in_loop = false;
            while (LFENCE, data[next].m.marker[id] == 0)
                if(++wait_cnt > 1000000 && !stuck_in_loop) {
                    std::cout << "C"<<id<<"[t"<<tail()<<"-n"<<next<< "]" << (int)data[next].m.marker[id] << std::endl;
                    stuck_in_loop = true;
                }
                LFENCE;
            //while (LFENCE, tail()==head());

            //std::cout << "-";
            //auto next = next_tail();
            //if(next == 0) std::cout <<"#";
            return data[next].d;
        }
    
        void finish() {
            inctail();
            SFENCE;
            clear_marker();
        }
    };

    LockFreeQueue3() : head_{0, 1}, data{} { std::cout<<"capacity="<<capacity<<", mask="<<std::hex<<MASK<<std::dec<<std::endl;};
    // auto issue has been fixed in 4.9.0

    Reader genReader()
    {
        std::lock_guard<std::mutex> lock(gen_reader_mutex);
        uint32_t cnt = reader_cnt.fetch_add(1);
        std::cout<<"creating reader-"<<cnt<<std::endl;
        return Reader(cnt, this->data);
        //return r;
    }

    uint64_t getMarkerMask() {
        static uint64_t maskset[] = {
            0x0000000000000000, 0x0000000000000001, 0x0000000000000101, 0x0000000000010101,
            0x0000000001010101, 0x0000000101010101, 0x0000010101010101, 0x0001010101010101,
            0x0101010101010101
        };
        return maskset[reader_cnt.load(std::memory_order_relaxed)];
    }
    T& reserve() {
        auto next = next_head();
        uint64_t wait_cnt = 0;
        bool stuck_in_loop = false;
        
        while(LFENCE, 
              data[next].m.marker64[0] )
            if(++wait_cnt>1000000 && !stuck_in_loop) {
                std::cout<<"P[h"<<head()<<"-n"<<next<<"]"<<std::hex<<data[next].m.marker64[0]<<std::dec<<"seq="<<data[next].d.a[0]<<std::endl;
                stuck_in_loop = true;
            }
              //next == tail_[0].val || next == tail_[1].val );
              //next == tail_[0].val );
        LFENCE;
        if(next ==0) std::cout<<"*";
        return data[next].d;
    }

    void poke() {
        head_ref()=head_.next;
        SFENCE;
        //asm volatile ("" : : : "memory");
        data[head()].m.marker64[0] = getMarkerMask();
        //if(next == 0) std::cout<<"head="<<next<<",marker="<<data[next].marker<<std::endl;
                
    }

};

#endif //__LOCKFREEQUEUE3_H__