#include <iostream>
#include <atomic>
#include <chrono>

#include <ThreadCpuAffinity.h>
#include <LockFreeQueue.h>
#include <LockFreeQueue2.h>
#include <LockFreeQueue3.h>

template<uint32_t N>
struct UserData
{
    uint64_t a[N];
};

//typedef long T;
typedef UserData<64> T;

//LockFreeQueue<T, 3000, 8> lfq;
//LockFreeQueue2<T, 3000, 8> lfq;
LockFreeQueue3<T, 3000, 8> lfq;


class Producer
{
public:
    void operator()() 
    {
        const uint32_t max_loop = 5000000;
        uint32_t count = 0;
        uint64_t seq = 0;
        auto start = std::chrono::system_clock::now();
        while(1) {
            auto & ud = lfq.reserve();
            ud.a[0] = ++seq;
            //++ud;
            lfq.poke();
            if(++count == max_loop) {
                auto diff = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start).count()/(1.0*max_loop);
                std::cout<<"produced - "<< lfq.capacity << ": " << diff << std::endl;
                count = 0;
                start = std::chrono::system_clock::now();
            }
        }
    }
};

class Consumer
{
public:
    void operator()()
    {
        uint64_t seq = 0;
        auto reader = lfq.genReader();
        while(1) {
            //const UserData &ud = lfq.peek();
            const auto &ud = reader.peek();
            if(ud.a[0] != ++seq) {
                std::cout<<"Consumer-"
                << ":Reordered data found, "
                << "expected ["<< seq <<"] "
                << "obtained ["<<ud.a[0]<<"]"
                << std::endl;
                return;
            }
            reader.finish();
        }
    }
};

int main(int argc, char **argv)
{
    jk::thread two {Consumer()}; two.setcpu(1);
    jk::thread t3  {Consumer()}; t3.setcpu(3);
    //jk::thread t4  {Consumer()}; t4.setcpu(2);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    jk::thread one {Producer()}; one.setcpu(0);
    //Consumer()();
    one.join();
    two.join();
    
    return 0;
}
