package biglittle.ipc.ring.ProducerConsumer;

import biglittle.ipc.ring.ReaderInf;
import biglittle.systemlib.CPUAffinity;
import biglittle.systemlib.NativeResource;

import java.util.ArrayList;
import java.util.List;

public class ConsumerProxy<T, P extends ProcessorInf<T>> implements Runnable {
    private List<ReaderInf> readers = new ArrayList<ReaderInf>();
    private P processor;
    private static final WaitStrategyInf BUSY_WAIT = new BusyWaitStrategy();
    private WaitStrategyInf waitStrategy = BUSY_WAIT;
    CPUAffinity cpuaffinity = NativeResource.getCPUAffinity();

    public ConsumerProxy(P p) {
        this.processor = p;
    }
    
    public ConsumerProxy(ReaderInf<T> r, P p) {
        readers.add(r);
        this.processor = p;
    }

    public ConsumerProxy(ReaderInf<T> r, P p, WaitStrategyInf waiter) {
        this(r, p);
        waitStrategy = waiter;
    }

    public void addReader(ReaderInf reader) {
        readers.add(reader);
    }

    public void setWaitStrategy(WaitStrategyInf waiter) {
        waitStrategy = waiter;
    }

    public void setAffinity(int cpu) {
        cpuaffinity.setAffinity(cpu);
    }

    @Override
    public void run() {
        cpuaffinity.setThreadHandle();
        cpuaffinity.setTask();
        subscribe();


        boolean loop = true;
        boolean any_ready = false;
        while (loop) {
            any_ready = false;
            for(ReaderInf<T> reader : readers) {
                if(reader.front(processor.getDataProxy(), false)) {
                    loop = processor.process();
                    if(!loop)
                        System.out.println("Stopping consumer at offset="+reader.getOffset());
                    //else
                    //    System.out.println("Processed at offset=" + reader.getOffset() + " seq=" + reader.getSequence());
                    reader.pop();
                    any_ready = true;
                }
            }
            if(!any_ready)
                waitStrategy.waitFor();
        }
    }

    //-- private ----------------------
    private void subscribe() {
        for(ReaderInf<T> reader : readers) {
            reader.subscribe();
        }
    }
}