package biglittle.ipc.ring.ProducerConsumer;

import biglittle.ipc.NotifierInf;
import biglittle.systemlib.NativeResource;

public class NotifierWaitStrategy implements WaitStrategyInf {
    NotifierInf notifier;
    public NotifierWaitStrategy(String name) {
        this.notifier = new NativeResource().getNamedNotifier(name);
    }

    @Override
    public void waitFor() {
        notifier.waitFor();
    }
    public void signal() {
        notifier.signal();
    }
    public void broadcast() {
        notifier.broadcast();
    }
}