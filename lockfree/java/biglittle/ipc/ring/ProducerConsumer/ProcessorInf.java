package biglittle.ipc.ring.ProducerConsumer;

public interface ProcessorInf<T> {
    public boolean process();
    public T getDataProxy();
    public boolean isSubscribed();
}