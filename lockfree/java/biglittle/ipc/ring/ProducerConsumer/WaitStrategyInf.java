package biglittle.ipc.ring.ProducerConsumer;

public interface WaitStrategyInf {
    public void waitFor();
}