package biglittle.ipc.ring.ProducerConsumer;

import biglittle.ipc.primitive.NativeMemory;

public class TimedWaitStrategy implements WaitStrategyInf {
    private long sleepTime;

    public TimedWaitStrategy(long nano) {
        sleepTime = nano;
    }
    @Override
    public void waitFor() {
        NativeMemory.UNSAFE.park(false, sleepTime);
    }
}