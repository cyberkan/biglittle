package biglittle.ipc.ring;

/**
 * Created by Dan on 11/19/2015.
 */
public interface ReaderInf<T> {
    boolean front(T rval);
    boolean front(T rval, boolean wait_loop);
    void pop();
    int getId();
    void subscribe();

    // just for test. delete it
    long getOffset();
    long getSequence();
}