package biglittle.ipc.ring;

import biglittle.ipc.StructureInArray;
import biglittle.ipc.primitive.NativeMemory;
import biglittle.systemlib.Utility;

public class LockFreeRingBuffer<T extends StructureInArray> extends NativeMemory {

    private static final long CACHE_LINE_SIZE  = 64;
    private static final long MAX_READERS      = 64;
    private static final long POSI_SIZE        = 8;
    private static final long DATA_MARKER_SIZE = 8;
    private static final long WDOG_MASK        = 0x00000000FFFFFFFFL;
    private static final long CMD_MASK         = 0x00000000000000FFL;
    private static final char CMD_PING         = 1;
    private static final char CMD_RELOAD_SUBS  = 2;

    //-- offsets --
    // 1st cache line
    private static final long WIDX_OFFSET   = 0;
    private static final long WMASK_OFFSET  = 8;
    private static final long SEQ_OFFSET    = 16;
    private static final long RECSZ_OFFSET  = 24;
    private static final long NUMREC_OFFSET = 32;

    // 2nd cache line
    private static final long SUBSMASK_OFFSET = 64;
    private static final long WDOG_OFFSET     = 72;
    private static final long RMARK_OFFSET    = 80;
    //private static final long WMASK_OFFSET  = 88;

    // 3rd cache line
    private static final long POSI_OFFSET = 128;

    // n cache lines
    private static final long DATA_OFFSET = Utility.roundTo64(POSI_OFFSET + POSI_SIZE * MAX_READERS);
    //---


    //----------------------------------

    public class Reader<T extends StructureInArray> extends NativeMemory implements ReaderInf<T> {
        private long mask;
        private long offset = 0;
        private long recSize = 0;
        private long segSize = 0;
        private long watchdog = 0;
        private int id;

        Reader(int id, long addr, long rsize, long segSize) {
            super(addr);
            this.id = id;
            this.recSize = rsize;
            this.segSize = segSize;
            this.mask = 1L << id;
            watchdog = getVolatileLong(WDOG_OFFSET) & WDOG_MASK;
            System.out.println("rec_size="+rsize);
        }

        @Override
        public boolean front(T rval) {
            return front(rval, true);
        }
        @Override
        public boolean front(T rval, boolean wait_loop) {
            long wdog = getVolatileLong(WDOG_OFFSET);
            long wver = wdog & WDOG_MASK;
            if(wver  != watchdog) {
                watchdog = wver;
                int cmd = (int)((wdog >> 32) & CMD_MASK);
                //System.out.println("Received WatchDog = "+wdog+", cmd="+cmd+", ver="+wver);
                switch (cmd) {
                    case CMD_PING:
                        fetchOrLong(RMARK_OFFSET, mask);
                        break;
                    case CMD_RELOAD_SUBS:
                        if((getVolatileLong(SUBSMASK_OFFSET) & mask) == 0)
                            throw new IllegalStateException("Forced to unsubscribe.");
                        break;
                }
            }
            ///
            int delayCnt = 0;
            while((getVolatileLong(offset) & mask) == 0) { // wait for data
                if(!wait_loop)
                    return false;
                else
                    Utility.busyDelay(++delayCnt);
            }
            //System.out.println("R"+mask+": front at "+offset+", seq="+getVolatileLong(offset+DATA_MARKER_SIZE));
            rval.rebase(base+offset+DATA_MARKER_SIZE);
            return true;
        }

        @Override
        public void pop() {
            // clear reader mark for this reader
            fetchAndLong(offset, ~mask);
            // move to next slot
            offset = Utility.addAndModulo(offset, recSize, DATA_OFFSET, segSize+DATA_OFFSET);
        }

        @Override
        public int getId() {
            return id;
        }

        @Override
        public void subscribe() {
            putOrderedLong(POSI_OFFSET+id*POSI_SIZE, -1); // clean Reader starting position to obtain from producer
            fetchOrLong(WMASK_OFFSET, mask);
            // wiat for the position
            long pos;
            while((pos = getVolatileLong(POSI_OFFSET+id*POSI_SIZE)) == -1);
            offset = pos;
            System.out.println("data starting position="+offset+"("+(offset/recSize)+") base="+pos);
        }

        // test only
        public long getOffset() {
            return offset;
        }
        public long getSequence() {
            return getVolatileLong(offset+DATA_MARKER_SIZE);
        }
    }



    //-----------------------------------

    public static long getMemorySizeFor(long elem_sz, long n) {
        long n2 = Utility.roundToPow2(n);
        long rsz = Utility.roundTo64(elem_sz+DATA_MARKER_SIZE);
        return Utility.roundTo64(DATA_OFFSET + rsz*n2);
    }

    //---------------------------------
    private long dataSegSize;
    private long numberElems = 0;
    private long recSz = 0;
    private long offset =0;
    private long writeMask = 0;

    //---------------------------------
    // for writer
    public LockFreeRingBuffer(long address, long elem_sz, long n) {
        super(address);
        this.numberElems = Utility.roundToPow2(n);
        putOrderedLong(NUMREC_OFFSET, this.numberElems);
        this.recSz = Utility.roundTo64(elem_sz + DATA_MARKER_SIZE);
        putOrderedLong(RECSZ_OFFSET, this.recSz);
        dataSegSize = Utility.roundTo64(recSz*numberElems);
        offset = getVolatileLong(WIDX_OFFSET);
        if(offset == 0) {
            offset = DATA_OFFSET;
            putOrderedLong(WIDX_OFFSET, offset);
        }
        writeMask = getVolatileLong(WMASK_OFFSET);
        System.out.println("# elems="+numberElems+", record size="+recSz+", data segment size="+dataSegSize);
    }
    // for readers
    public LockFreeRingBuffer(long address) {
        super(address);
        this.numberElems = getVolatileLong(NUMREC_OFFSET);
        this.recSz = getVolatileLong(RECSZ_OFFSET);
        dataSegSize = Utility.roundTo64(recSz*numberElems);
        offset = getVolatileLong(WIDX_OFFSET);
        writeMask = getVolatileLong(WMASK_OFFSET);
    }

    // reader threads/processes call genReader, therefore use getVolatile* functions
    public Reader genReader() {
        int id = findEmptyReaderAndSeize();
        if(id < 0)
            throw new RuntimeException("No space to add new reader");

        return new Reader(id,
                          base,  // beginning point of memory
                          recSz,
                          dataSegSize
                   ); // data record size
    }

    // producer functions
    public boolean reserve(T rval) {
        return reserve(rval, true);
    }
    public boolean reserve(T rval, boolean wait_loop) {
        long marker = 0;
        processSubscription();
        while(writeMask == 0 || ((marker = getVolatileLong(offset)) & writeMask) != 0) { // busy wait for empty slot
            processSubscription();
            if(writeMask != 0)
                handleSlowReaders(marker);
            else {
                clearWatchDog();
                //NativeMemory.usleep(1);
            }
            if(!wait_loop)
                return false;
        }
        clearWatchDog();
        //System.out.println("Reserved at "+offset+", seq="+getVolatileLong(offset+DATA_MARKER_SIZE));
        rval.rebase(base+offset+DATA_MARKER_SIZE);
        return true;
    }
    public void commit() {
        // set the data ready marker
        //putOrderedLong(getVolatileLong(WIDX_OFFSET), getVolatileLong(WMASK_OFFSET));
        //System.out.println("ENTER to commit at "+offset+" with seq="+getVolatileLong(offset+DATA_MARKER_SIZE));
        //Utility.pressEnter();
        putVolatileLong(offset, writeMask);

        // move to the next slot
        offset = Utility.addAndModulo(offset, recSz, DATA_OFFSET, dataSegSize+DATA_OFFSET);
        putOrderedLong(SEQ_OFFSET, getVolatileLong(SEQ_OFFSET) + 1); // ToDo: may not the best way to save seq due to the integrity
        putOrderedLong(WIDX_OFFSET, offset);

    }

    public long getSequenceNumber() {
        return getVolatileLong(SEQ_OFFSET);
    }
    public int getNumberOfReaders() {
        long mask = getVolatileLong(SUBSMASK_OFFSET);
        int cnt = 0;
        for(int i =0; i < MAX_READERS; ++i) {
            long selected = (1L << i);
            if((mask & selected) != 0L)
                ++cnt;
        }
        return cnt;
    }
    public boolean emptyReader() {
        return getVolatileLong(SUBSMASK_OFFSET) == 0L;
    }

    //----------------------------
    private int findEmptyReaderAndSeize() {
        long mask;
        for(int i =0; i < MAX_READERS; ++i) {
            long selected = (1L << i);
            if(((mask = getVolatileLong(SUBSMASK_OFFSET)) & selected) == 0) {
                if(compareAndSwapLong(SUBSMASK_OFFSET, mask, mask | selected))
                    return i;
                else { // failed to seize : try all again
                    i = 0;
                }
            }
        }
        return -1;   // already maxed.
    }
    private void processSubscription() {
        long newMask = getVolatileLong(WMASK_OFFSET);
        if(writeMask != newMask) {
            long changed = newMask & ~writeMask;
            for(int i = 0; changed != 0; ++i) {
                long mask = 1L << i;
                if((changed & mask) != 0 ) {
                    changed &= ~mask;
                    putOrderedLong(POSI_OFFSET+i*POSI_SIZE, offset);
                    System.out.println("processed new subs id="+i+", posi="+offset+", seq="+getVolatileLong(SEQ_OFFSET));
                }
            }
            writeMask = newMask;
        }
    }

    private long startWatchDogTimer;
    private long watchDogCount = 0;
    private static final long MAX_WATCHDOG_COUNT = Long.parseLong(System.getProperty("WC", "100000"));
    private static final long MAX_WATCHDOG_TIME = Long.parseLong(System.getProperty("WT", "100"))*1000000;  // 100 msec

    private void handleSlowReaders(long slowReaderMask) {
        ++watchDogCount;
        if(watchDogCount == MAX_WATCHDOG_COUNT) { // first time
            startWatchDogTimer = System.nanoTime();
            requestReaderMark();
            //System.out.println("start watchdog timer...");
            kickOutSlowReader(slowReaderMask);
        }
        else if(watchDogCount > MAX_WATCHDOG_COUNT && (System.nanoTime()-startWatchDogTimer) > MAX_WATCHDOG_TIME) {
                deleteUnresponsiveReader();
                watchDogCount = 0;
                startWatchDogTimer = System.nanoTime();
        }
    }
    private void clearWatchDog() {
        watchDogCount = 0;
    }
    // command must be 1~255
    private void sendWatchDogCommand(long cmd) {
        long wdog = getLong(WDOG_OFFSET);
        wdog = (wdog+1) & WDOG_MASK;
        wdog |= (cmd & CMD_MASK) << 32;
        putOrderedLong(WDOG_OFFSET, wdog);
        System.out.println("Sent WatchDog cmd = "+cmd+ ", wdog="+(wdog&WDOG_MASK));
    }
    private void requestReaderMark() {
        putOrderedLong(RMARK_OFFSET, 0);
        sendWatchDogCommand(CMD_PING);
    }
    private long[] slowReaderTimeMark = new long[(int)MAX_READERS];
    private long[] slowReaderCounter = new long[(int)MAX_READERS];
    private long slowReaderCheckInterval = Long.parseLong(System.getProperty("SI","60"));
    private long slowReaderAllowedMaxCounter = Long.parseLong(System.getProperty("SC", "10"));
    
    private void kickOutSlowReader(long slowReaderMask) {
        long newMask = 0;
        for(int i = 0; i < MAX_READERS; ++i) {
            long mask = (1L << i);
            if((slowReaderMask & mask) != 0) {
                long now = System.nanoTime();
                if((now - slowReaderTimeMark[i]) > slowReaderCheckInterval*1000000000L) {
                    slowReaderTimeMark[i] = now;
                    slowReaderCounter[i] = 1;
                }
                else if(slowReaderCounter[i] > slowReaderAllowedMaxCounter) {
                    newMask |= mask;
                    slowReaderTimeMark[i] = 0;
                    slowReaderCounter[i] = 0;
                    System.out.println("Kick out slow reader " +i+" = "+mask);
                }
                else
                    ++slowReaderCounter[i];
            }
        }
        if(newMask != 0) {
            System.out.println("KickOut mask = " + newMask);
            deleteUnresponsiveReader(true, writeMask & ~newMask);
        }
    }
    private void deleteUnresponsiveReader() {
        deleteUnresponsiveReader(false, 0);
    }
    private void deleteUnresponsiveReader(boolean useGivenMask, long newMask) {
        long start = System.nanoTime();
        long writeMask;
        do {
            writeMask = getVolatileLong(SUBSMASK_OFFSET);
            if(!useGivenMask)
                newMask = getVolatileLong(RMARK_OFFSET);
        } while(!compareAndSwapLong(SUBSMASK_OFFSET, writeMask, newMask));
        putOrderedLong(WMASK_OFFSET, newMask);
        sendWatchDogCommand(CMD_RELOAD_SUBS);
        this.writeMask = newMask;

        for(int i = 0; i < MAX_READERS; ++i) {
            long mask = (1L << i);
            if((mask & newMask) == 0)
                cleanupReaderMark(~mask);
        }
        double diff = (System.nanoTime() - start)/(1.0*1000000);
        System.out.println("Delete zombie readers took "+diff+" msec at "+offset+", newMask="+newMask+", seq="+getVolatileLong(SEQ_OFFSET));
    }
    private void cleanupReaderMark(long mask) {
        for(long pos = DATA_OFFSET; pos < dataSegSize+DATA_OFFSET; pos += recSz)
            fetchAndLong(pos, mask);
        // print reader mask to check
        //for(long pos = DATA_OFFSET; pos < dataSegSize+DATA_OFFSET; pos += recSz)
        //    System.out.print(getVolatileLong(pos)+",");
        //System.out.println("");
    }
}