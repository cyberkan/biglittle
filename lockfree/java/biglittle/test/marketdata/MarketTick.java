package biglittle.test.marketdata;

import biglittle.ipc.StructureInArray;
import biglittle.ipc.primitive.NativeMemory;

public class MarketTick extends NativeMemory implements StructureInArray {
    public static final long SIZE = 32;
    private static final long PRICE_OFFSET = 0;
    private static final long QUANTITY_OFFSET = 8;
    private static final long TIME_OFFSET = 16;
    private static final long MARKET_OFFSET = 24;  // 8 bytes

    @Override
    public long size() {
        return SIZE;
    }

    public double getPrice() {
        return getVolatileDouble(PRICE_OFFSET);
    }

    public double getQuantity() {
        return getVolatileDouble(QUANTITY_OFFSET);
    }

    public long getTimeStamp() {
        return getVolatileLong(TIME_OFFSET);
    }

    public void putPrice(double price) {
        putVolatileDouble(PRICE_OFFSET, price);
    }

    public void putQuantity(double quantity) {
        putVolatileDouble(QUANTITY_OFFSET, quantity);
    }

    public void putTimeStamp(long time) {
        putOrderedLong(TIME_OFFSET, time);
    }

    public long getMarketCode() {
        return getVolatileLong(MARKET_OFFSET);
    }

    public void putMarketCode(long mkt) {
        putOrderedLong(MARKET_OFFSET, mkt);
    }

}
