package biglittle.test.marketdata;

import biglittle.ipc.primitive.SharedCache;
import biglittle.ipc.primitive.NativeMemory;

public class MarketDataRealTimeEventProcessor extends MarketDataEventProcessor {
    private MarketProduct mp = new MarketProduct();
    private MarketTick open = new MarketTick();
    private MarketTick close = new MarketTick();
    private MarketTick last = new MarketTick();
    private MarketTick nbbid = new MarketTick();
    private MarketTick nbask = new MarketTick();
    private Tick bbid = new Tick();
    private Tick bask = new Tick();


    private SharedCache<MarketProduct> tickCache;
    private long seq = 0;
    private boolean seqSynced = false;
    private int id;
    // measurement
    private long start=0;
    private long cnt=0;
    private long delay;
    private int prevProdId = 0;

    public MarketDataRealTimeEventProcessor(int id, SharedCache<MarketProduct> tickCache) {
        this.id = id;
        this.delay = 0;
        this.tickCache = tickCache;
    }

    public MarketDataRealTimeEventProcessor(int id, long delay, SharedCache<MarketProduct> tickCache) {
        this.id = id;
        this.delay = delay;
        this.tickCache = tickCache;
    }

    @Override
    public boolean process() {

        long s = mde.getSequence();
        long e = mde.getEvent();
        int evt = mde.getEventType(e);

        ++seq; ++cnt;

        if(!seqSynced) {  // initial attach to queue
            seq = s;
            seqSynced = true;
            if(start == 0) {
                System.out.println("Starting with sequence="+s);
                start = System.nanoTime();
            }
        }
        int prodId =  mde.getProductId(e);
        try {
            tickCache.seek(mp, prodId);
        }
        catch (IllegalArgumentException ex) {
            System.out.printf("%s: received event=0x%08X",ex, e);
        }
        //tickCache.seek(mp, s & 0xFFF);
        //tickCache.seek(mp, ((s << 4) | ((s >> 4)&0x0F)) & 0xFFL);
        double diff;
        switch(evt) {
            case 63: //no operation
                break;
            case 62: // exit
                diff = (System.nanoTime() - start)/(1.0*cnt);
                System.out.println("C"+id+" is exiting with seq="+seq+", nano/loop="+diff);
                return false;
            case 60:  // start measurement
                seq = s;
                //System.out.println("start new measurement with seq="+seq);
                cnt = 0;
                start = System.nanoTime();
                break;
            case 61: // end measurement
                diff = (System.nanoTime() - start)/(1.0*cnt);
                System.out.println("C"+id+" is running with seq="+seq+", cnt="+cnt+", nano/loop="+diff);
                cnt = 0;
                break;
            case 0:
                mp.loadOpenTick();
                mp.getOpenTick(open);
                break;
            case 1:
                mp.loadCloseTick();
                mp.getCloseTick(close);
                break;
            case 2:
                mp.loadLastTradeTick();
                mp.getLastTradeTick(last);
                break;
            case 3:
                mp.loadNBBO();
                mp.getNBBO(nbbid, nbask);
                break;
            case 4:
                mp.loadBBOMarket();
                mp.getBBOMarket(0, bbid, bask);
                break;
            case 5:
                if((prodId >> 3) == prevProdId)
                    break;
                prevProdId = prodId >> 4;
            default:
                mp.loadAll();
                mp.getOpenTick(open);
                mp.getCloseTick(close);
                mp.getLastTradeTick(last);
                mp.getNBBO(nbbid, nbask);
                mp.getBBOMarket(0, bbid, bask);
        }

        //if(open.getPrice() != s/10000.0)
        //    System.out.println("open price is not synced");
        //if(open.getMarketCode() != 5)
        //    System.out.println("open market code is not synced");
        //if(open.getQuantity() != 100)
        //    System.out.println("open quantity mismatched");
        //if(open.getTimeStamp() != s)
        //    System.out.println("open time mismatched");
        //if(id==3) {
        //    NativeMemory.nanosleep(1);
        //}


        if(s != seq) {
            System.out.println("Sequence of MDE is not matched at "+cnt+" : expected="+seq+", received="+s);
            return false;
        }

        if(delay != 0)
            NativeMemory.usleep(delay);
        return true;
    }

    @Override
    public boolean isSubscribed() {
        return false;
    }

}
