package biglittle.test.marketdata;

import biglittle.ipc.ring.ProducerConsumer.ProcessorInf;

public abstract class MarketDataEventProcessor implements ProcessorInf<MarketDataEvent> {
    protected MarketDataEvent mde = new MarketDataEvent();

    @Override
    public MarketDataEvent getDataProxy() {
        return mde;
    }
}