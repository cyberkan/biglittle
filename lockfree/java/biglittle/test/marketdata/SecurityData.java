package biglittle.test.marketdata;

import biglittle.ipc.StructureInArray;
import biglittle.ipc.primitive.NativeMemory;

public class SecurityData extends NativeMemory implements StructureInArray {

    public static final long SIZE = 24;

    public static final long INACTIVE = 0;
    public static final long ACTIVE = 1;
    public static final long UNSUBSCRIBED = 2;
    public static final long INTERESTED = 3;
    public static final long SUBSCRIBING = 4;
    public static final long SUBSCRIBED = 5;
    public static final long UNSUBSCRIBING = 6;

    public static final long HALTED = 9;
    public static final long MAX_STATUS = 10;

    private static final long ID_OFFSET = 0;
    private static final long VER_OFFSET = 8;
    private static final long STATUS_OFFSET = 16;


    @Override
    public void rebase(long addr) {
        super.rebase(addr);
    }

    @Override
    public long size() {
        return SIZE;
    }

    public long getId() {
        return getVolatileLong(ID_OFFSET);
    }

    public long getStatus() {
        return getVolatileLong(STATUS_OFFSET);
    }

    public long getVersion() {
        return getVolatileLong(VER_OFFSET);
    }

    public void setId(long id) {
        putOrderedLong(ID_OFFSET, id);
    }

    public long activate() {
        return setStatus(ACTIVE, INACTIVE, ACTIVE);
    }
    public long interested() {
        return setStatus(INTERESTED, ACTIVE, INTERESTED);
    }

    public long subscribing() {
        return setStatus(SUBSCRIBING, ACTIVE, INTERESTED);
    }
    public long subscribed() {
        return setStatus(SUBSCRIBED, SUBSCRIBING, SUBSCRIBING );
    }

    public long unsubscribing() {
        return setStatus(UNSUBSCRIBING, SUBSCRIBED, SUBSCRIBED);
    }

    public long unsubscribed() {
        return setStatus(UNSUBSCRIBED, UNSUBSCRIBING, UNSUBSCRIBING);
    }


    public long increaseVersion() {
        return fetchAndAddLong(VER_OFFSET, 1);
    }

    //---------------
    private long setStatus(long status, long l, long r) {
        long curStatus;
        long newStatus;
        do {
            curStatus = getVolatileLong(STATUS_OFFSET);
            if(curStatus >= l && curStatus <= r)
                newStatus = status;
            else {
                newStatus = curStatus;
                break;
            }
        } while(!compareAndSwapLong(STATUS_OFFSET, curStatus, newStatus));

        return newStatus;
    }
}
