package biglittle.test.marketdata;

import biglittle.ipc.StructureInArray;
import biglittle.ipc.primitive.NativeMemory;

public class MarketDataEvent extends NativeMemory implements StructureInArray {

    public static final long SIZE = 16;

    private static final long SEQ_OFFSET = 0;
    private static final long EVENT_OFFSET = 8;

    private static final int productId_bits    = 16; //
    private static final int marketEvType_bits = 6;  //
    private static final int dataVer_bits      = 42; //

    public static final long productIdMask = initializeMask(productId_bits);
    public static final long marketEvTypeMask = initializeMask(marketEvType_bits);
    public static final long dataVerMask = initializeMask(dataVer_bits);

    private static long initializeMask(int bits) {
        long mask = 0;
        for(int i=0; i < bits; ++i)
            mask |= (1L << i);
        return mask;
    }

    public static int getProductId(long ev) {
        return (int)((ev >> (marketEvType_bits + dataVer_bits)) & productIdMask);
    }
    public static int getEventType(long ev) {
        return (int)((ev >> (dataVer_bits)) & marketEvTypeMask);
    }
    public static long getDataVersion(long ev) {
        return ev & dataVerMask;
    }
    public static long makeEvent(int prodId, int etype, long ver) {
        long event = prodId & productIdMask;
        event = (event << marketEvType_bits) | (etype & marketEvTypeMask);
        event = (event << dataVer_bits) | (ver & dataVerMask);
        return event;
    }

    //-----------------------------
    @Override
    public void rebase(long addr) {
        super.rebase(addr);
    }

    @Override
    public long size() {
        return SIZE;
    }

    public long getSequence() {
        return getVolatileLong(SEQ_OFFSET);
    }
    public void setSequence(long seq) {
        putOrderedLong(SEQ_OFFSET, seq);
    }
    public long getEvent() {
        return getVolatileLong(EVENT_OFFSET);
    }
    public void setEvent(long val) {
        putOrderedLong(EVENT_OFFSET, val);
    }

}
