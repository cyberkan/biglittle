package biglittle.test.marketdata;

import biglittle.ipc.StructureInArray;
import biglittle.ipc.primitive.NativeMemory;
import biglittle.systemlib.Utility;

public class MarketProduct extends NativeMemory implements StructureInArray {
    // test.marketdata.SecurityData + OpenTick + CloseTick + LastTradeTick + NBBO + BBO + PriceLevels
    private static final int NUM_MARKERS = 6;
    private static final int MARKER_SIZE = 8;
    // total MarketTicks = OpenTick + CloseTick + LastTradeTick
    //                     + NBBO.Bid + NBBO.Ask
    //                     + BBO(Bid+Ask) * N_BBO_MARKETS 15 (should be config)
    //                     + PL10 (Bid/Ask 10 levels) * NPL_MARKETS 5 (config)
    private static final long N_BBO_MARKETS = 15;
    private static final long N_PL_MARKETS = 4;
    private static final long N_PRICE_LVL = 3;

    private static final long SECDATA_OFFSET = 0;
    private static final long MARKER_OFFSET = SecurityData.SIZE;
    private static final long VER_OFFSET = MARKER_OFFSET + Long.SIZE/8;
    private static final long OPENTICK_OFFSET = VER_OFFSET + Long.SIZE/8;
    private static final long CLOSETICK_OFFSET = OPENTICK_OFFSET + MARKER_SIZE;
    private static final long LASTTRADE_OFFSET = CLOSETICK_OFFSET + MarketTick.SIZE;
    private static final long NBBO_OFFSET = LASTTRADE_OFFSET + MarketTick.SIZE;
    private static final long BBO_OFFSET = NBBO_OFFSET + MarketTick.SIZE * 2; // bid, ask
    private static final long PL_OFFSET = BBO_OFFSET + Tick.SIZE * 2 * N_BBO_MARKETS;

    //public static final long SIZE = PL_OFFSET + test.marketdata.Tick.SIZE * 2 * N_PL_MARKETS * N_PRICE_LVL;
    //private long localBuffer = UNSAFE.allocateMemory(SIZE);
    public static final long SIZE = Utility.roundToPow2(PL_OFFSET + Tick.SIZE * 2 * N_PL_MARKETS * N_PRICE_LVL);
    private long localBuffer = UNSAFE.allocateMemory(Utility.roundToPow2(SIZE));

    @Override
    public long size() {
        return SIZE;
    }

    public SecurityData getSecurityData(SecurityData secData) {
        secData.rebase(base+SECDATA_OFFSET);
        return secData;
    }

    public MarketTick getOpenTick(MarketTick mtick) {
        mtick.rebase(localBuffer+OPENTICK_OFFSET);
        return mtick;
    }

    public void loadOpenTick() {
        loadVolatile(OPENTICK_OFFSET, MarketTick.SIZE);
    }

    public MarketTick getCloseTick(MarketTick mtick) {
        mtick.rebase(localBuffer+CLOSETICK_OFFSET);
        return mtick;
    }

    public void loadCloseTick() {
        loadVolatile(CLOSETICK_OFFSET, MarketTick.SIZE);
    }

    public MarketTick getLastTradeTick(MarketTick mtick) {
        mtick.rebase(localBuffer+LASTTRADE_OFFSET);
        return mtick;
    }

    public void loadLastTradeTick() {
        loadVolatile(LASTTRADE_OFFSET, MarketTick.SIZE);
    }

    public void getNBBO(MarketTick bid, MarketTick ask) {
        bid.rebase(localBuffer+NBBO_OFFSET);
        ask.rebase(localBuffer+NBBO_OFFSET+ MarketTick.SIZE);
    }

    public void loadNBBO() {
        loadVolatile(NBBO_OFFSET, MarketTick.SIZE * 2);
    }

    // ToDo: test implementation. have to re-write.
    public void getBBOMarket(int mkt, Tick bid, Tick ask) {
        long quoteSize = Tick.SIZE * 2;
        bid.rebase(localBuffer+BBO_OFFSET+mkt*quoteSize);
        ask.rebase(localBuffer+BBO_OFFSET+mkt*quoteSize+ Tick.SIZE);
    }

    public void loadBBOMarket() {
        long quoteSize = Tick.SIZE * 2;
        loadVolatile(BBO_OFFSET, quoteSize*N_BBO_MARKETS);
    }

    public void getPriceLevelMarket(int mkt, int level, Tick bid, Tick ask) {
        long quoteSize = Tick.SIZE * 2;
        bid.rebase(localBuffer+PL_OFFSET+mkt*level*quoteSize);
        ask.rebase(localBuffer+PL_OFFSET+mkt*level*quoteSize+ Tick.SIZE);
    }

    public void loadPriceLevelMarket() {
        long quoteSize = Tick.SIZE * 2;
        loadVolatile(PL_OFFSET, quoteSize*N_PL_MARKETS*N_PRICE_LVL);
    }

    public void acquire() {
        acquire(MARKER_OFFSET);
    }

    public void commit() {
        release(MARKER_OFFSET);
    }

    // ToDo: End

    public void loadAll() {
        // load as a big block
        //loadVolatile(VER_OFFSET, SIZE-VER_OFFSET);

        // load each segments instead
        loadVolatile(VER_OFFSET, BBO_OFFSET-VER_OFFSET);
        loadVolatile(BBO_OFFSET, PL_OFFSET-BBO_OFFSET);
        loadVolatile(PL_OFFSET, SIZE-PL_OFFSET);

    }
    //---------------------------------

    private void loadVolatile(long offset, long size) {
        long marker;
        long retryCnt = 0;
        long retryCnt2 = 0;
        long src = base+offset;
        long dest = localBuffer+offset;
        do {
            // if the data is in the middle of update, wait for the completion
            //retryCnt = 0;
            do {
                marker = getVolatileLong(MARKER_OFFSET);
                ++retryCnt;
                //delay a bit instead of reading ipc immediately
            } while(!isReleased(marker) && Utility.busyDelay((int)retryCnt*2));

            memcopy(src, dest, size);
            ++retryCnt2;
        } while(marker != getVolatileLong(MARKER_OFFSET) /*&& doDelay(retryCnt2)*/); // if the data has been updated while reading, re-read
        //if(retryCnt2 > 1) {
            //System.out.println("retried to load "+retryCnt2+" times with inter-retry "+retryCnt+".");
        //}
    }


}
