package biglittle.test.marketdata;

import biglittle.ipc.primitive.SharedCache;
import biglittle.ipc.primitive.DirtyRecordTable;

public class MarketDataConflatedEventProcessor extends MarketDataEventProcessor {
    DirtyRecordTable dirtyDatatable;
    int id;
    long cnt = 0;
    private long start=0;

    public MarketDataConflatedEventProcessor(int id, SharedCache<MarketProduct> tickCache, DirtyRecordTable ddt) {
        dirtyDatatable = ddt;
        this.id = id;
    }

    @Override
    public boolean process() {
        ++cnt;
        long seq = mde.getSequence();
        long e = mde.getEvent();
        int evt = mde.getEventType(e);
        int prod = mde.getProductId(e);
        long ver = mde.getDataVersion(e);

        // test for how long take to overflow 40 bits of version field
        if(ver == mde.dataVerMask) {
            System.out.println("Event.dataVer is overflowed = "+ver);
            return false;
        }

        dirtyDatatable.markDirty(prod);
        //System.out.println("done for "+prod);

        double diff;
        switch(evt) {
            case 63: //no operation
                break;
            case 62: // exit
                diff = (System.nanoTime() - start)/(1.0*cnt);
                System.out.println("C"+id+" is exiting with seq="+seq+", nano/loop="+diff);
                return false;
            case 60:  // start measurement
                //System.out.println("start new measurement with seq="+seq);
                cnt = 0;
                start = System.nanoTime();
                break;
            case 61: // end measurement
                diff = (System.nanoTime() - start)/(1.0*cnt);
                System.out.println("C"+id+" is running with seq="+seq+", cnt="+cnt+", nano/loop="+diff);
                cnt = 0;
                break;

            default:

        }
        return true;
    }

    @Override
    public boolean isSubscribed() {
        return false;
    }
}
