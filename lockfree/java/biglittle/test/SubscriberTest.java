package biglittle.test;

import biglittle.ipc.NamedMemoryInf;
import biglittle.ipc.NotifierInf;
import biglittle.ipc.primitive.SharedCache;
import biglittle.systemlib.NativeResource;
import biglittle.test.marketdata.MarketProduct;
import biglittle.test.marketdata.SecurityData;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class SubscriberTest {
    public static void main(String atgs[]) {

        NamedMemoryInf memSource = NativeResource.getNamedSharedMemory("MarketData", false);
        long mem = memSource.attach("Event");
        long memMk = memSource.attach("TickCache");
        SharedCache<MarketProduct> tickCache = new SharedCache<>(memMk);
        long numRecs = tickCache.capacity();
        System.out.println("TickCache capacity="+numRecs);
        NotifierInf noti = NativeResource.getNamedNotifier("SubscriptionNotification");

        MarketProduct mp = new MarketProduct();
        SecurityData sec = new SecurityData();

        while(true) {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            try {
                System.out.print("Enter ProductId to subscribe: ");
                String cmds[] = in.readLine().split(" ");
                if(cmds.length < 1) {
                    System.out.println("Usage: subs/unsubs <productId>");
                    continue;
                }
                if(cmds.length == 1) {
                    if(cmds[0].equalsIgnoreCase("list"))
                        list(tickCache, mp, sec);
                    else
                        System.out.println("Usage:\nlist\nsubs/unsubs <productId>");
                }
                else if(cmds.length == 2) {
                    int pid = Integer.parseInt(cmds[1]);
                    if(pid > numRecs) {
                        System.out.println("ProductId is too big. Enter lower than "+numRecs);
                        continue;
                    }
                    tickCache.seek(mp, pid);
                    if(cmds[0].equalsIgnoreCase("subs"))
                        mp.getSecurityData(sec).interested();
                    else if(cmds[0].equalsIgnoreCase("unsubs"))
                        mp.getSecurityData(sec).unsubscribing();

                }
                noti.signal();

            } catch (Exception e) {
                System.out.println("Entered invalid productId, please enter an id lower than " + numRecs);
                ;
            }
        }

    }

    //-----------
    private static void list(SharedCache<MarketProduct> cache, MarketProduct mp, SecurityData sec) {
        for(int i= 0; i < cache.capacity(); ++i) {
            cache.seek(mp, i);
            if(mp.getSecurityData(sec).getStatus() >= sec.SUBSCRIBED)
                System.out.println("product ["+i+ "] has been subscribed");
        }

    }

}
