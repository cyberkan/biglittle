package biglittle.test;

import biglittle.ipc.NamedMemoryInf;
import biglittle.ipc.NotifierInf;
import biglittle.ipc.ring.ProducerConsumer.ConsumerProxy;
import biglittle.ipc.ring.ProducerConsumer.NotifierWaitStrategy;
import biglittle.ipc.primitive.SharedCache;
import biglittle.ipc.primitive.DirtyRecordTable;
import biglittle.ipc.primitive.NativeMemory;
import biglittle.ipc.ring.LockFreeRingBuffer;
import biglittle.systemlib.CPUAffinity;
import biglittle.systemlib.NativeResource;
import biglittle.test.marketdata.*;

public class ConsumerTest {

    private static final int cpu = Integer.parseInt(System.getProperty("C", "-2"));
    private static final int delay = Integer.parseInt(System.getProperty("D", "0"));
    private static final boolean subsEvent = Boolean.parseBoolean(System.getProperty("SE", "true"));
    private static final boolean subsNotifier = Boolean.parseBoolean(System.getProperty("SN", "false"));
    private static final boolean subsConflatedEvent = Boolean.parseBoolean(System.getProperty("SCE", "false"));
    private static final int ConflatedReaderCpu = Integer.parseInt(System.getProperty("SCC", "0"));

    public static void main(String args[]) {

        NamedMemoryInf memSource = NativeResource.getNamedSharedMemory("MarketData", false);
        long mem = memSource.attach("Event");
        LockFreeRingBuffer<MarketDataEvent> rb = new LockFreeRingBuffer<>(mem);

        long memMk = memSource.attach("TickCache");
        SharedCache<MarketProduct> tickCache = new SharedCache<>(memMk);
        long numRecs = tickCache.capacity();
        System.out.println("TickCache capacity="+numRecs);
        NotifierInf noti = NativeResource.getNamedNotifier("SubscriptionNotification");
        subscribeAll(tickCache, noti);
        ConsumerProxy<MarketDataEvent,MarketDataEventProcessor> c;
        int id = 0;
        DirtyRecordTable ddt = null;
        if(subsEvent) {
            LockFreeRingBuffer.Reader reader = rb.genReader();
            id = reader.getId();
            MarketDataEventProcessor mdeProc;
            if(!subsConflatedEvent)
                 mdeProc = new MarketDataRealTimeEventProcessor(id, delay, tickCache);
            else {
                ddt = new DirtyRecordTable(numRecs);
                mdeProc = new MarketDataConflatedEventProcessor(id, tickCache, ddt);
            }
            c = new ConsumerProxy<>(reader, mdeProc);
            if(subsNotifier)
                c.setWaitStrategy(new NotifierWaitStrategy("NotificationForConsumerOfLFQ"));
            Thread t = new Thread(c);
            if(cpu >= 0)
                c.setAffinity(cpu);
            System.out.println("Starting a consumer-" + id + " on CPU=" + cpu);
            t.start();
            if(subsConflatedEvent)
                workWithProduct(ConflatedReaderCpu, tickCache, ddt);
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else {
            workWithProduct(cpu, tickCache, ddt);
        }

    }

    private static void workWithProduct(int cpu, SharedCache<MarketProduct> tickCache, DirtyRecordTable ddt) {
        MarketProduct mp = new MarketProduct();
        SecurityData sec = new SecurityData();
        MarketTick open = new MarketTick();
        MarketTick close = new MarketTick();
        MarketTick last = new MarketTick();
        MarketTick nbbid = new MarketTick();
        MarketTick nbask = new MarketTick();
        Tick bbid = new Tick();
        Tick bask = new Tick();
        CPUAffinity cpuaffinity = NativeResource.getCPUAffinity();

        if(cpu >= 0) {
            cpuaffinity.setAffinity(cpu);
            cpuaffinity.setThreadHandle();
            cpuaffinity.setTask();
        }

        System.out.println("Starting conflated data consumer at CPU-"+cpu);

        long seq = 0, cnt = 0;
        long start = System.nanoTime();
        long prod;
        while(true) {
            if(ddt != null) {
                prod = ddt.getDirtyRecord();
                if(prod < 0) {
                    // no more dirty product to read
                    ddt.flip();
                    NativeMemory.usleep(delay);
                    continue;
                }
                //NativeMemory.nanosleep(delay);
            }
            else
                prod = ((seq << 4) | ((seq >> 4)&0x0FL)) & 0xFFL;
            tickCache.seek(mp, prod);
            //mp.loadOpenTick(open);
            mp.loadAll();
            //mp.getOpenTick(open);
            //mp.getCloseTick(close);
            //mp.getLastTradeTick(last);
            //mp.getNBBO(nbbid, nbask);
            //mp.getBBOMarket(0, bbid, bask);
            //if(open.getPrice() != s/10000.0)
            //    System.out.println("open price is not synced");
            //if(open.getMarketCode() != 5)
            //    System.out.println("open market code is not synced");
            //if(open.getQuantity() != 100)
            //    System.out.println("open quantity mismatched");
            //if(open.getTimeStamp() != s)
            //    System.out.println("open time mismatched");
            //if(id==3) {
            //    NativeMemory.nanosleep(1);
            //}
            ++seq;
            ++cnt;
            if(cnt == 1000000) {  // complete on batch of measurement
                double diff = (System.nanoTime() - start)/(1.0*cnt);
                System.out.println("C*"+" is running with seq="+seq+", nano/loop="+diff);
                cnt = 0;
                start = System.nanoTime();
            }
        }
    }

    private static void subscribeAll(SharedCache<MarketProduct> cache, NotifierInf noti) {
        MarketProduct mp = new MarketProduct();
        SecurityData sec = new SecurityData();
        long size = cache.capacity();
        for(long i = 0; i < size; ++i) {
            cache.seek(mp, i);
            mp.getSecurityData(sec).interested();
        }
        System.out.println("notify subscriptions "+size+" products");
        noti.signal();
    }
}
