package biglittle.test;

import biglittle.ipc.NamedMemoryInf;
import biglittle.ipc.ring.ProducerConsumer.ConsumerProxy;
import biglittle.ipc.ring.ProducerConsumer.NotifierWaitStrategy;
import biglittle.ipc.primitive.SharedCache;
import biglittle.ipc.ring.LockFreeRingBuffer;
import biglittle.ipc.ring.ReaderInf;
import biglittle.systemlib.CPUAffinity;
import biglittle.systemlib.NativeResource;
import biglittle.systemlib.Utility;
import biglittle.test.marketdata.*;

import java.util.ArrayList;
import java.util.List;

public class ProducerTest {
    private static final long RING_SIZE = 4096*64;
    private static final long MAX_PRODUCTS = 5000;

    private static boolean  cleanupRequired = Boolean.parseBoolean(System.getProperty("C", "false"));
    private static long loop = Long.parseLong(System.getProperty("N", "10000000"));
    private static boolean waitLoop = Boolean.parseBoolean(System.getProperty("PW", "true"));
    private static boolean notifyConsumer = Boolean.parseBoolean(System.getProperty("PN", "false"));
    private static boolean publishEvent = Boolean.parseBoolean(System.getProperty("PE", "true"));
    private static long prodSet = Long.parseLong(System.getProperty("PS", "4095"));
    private static int requestedEventType = Integer.parseInt(System.getProperty("PET", "-1"));

    private static MarketDataEvent mde = new MarketDataEvent();
    private static MarketProduct mp = new MarketProduct();
    private static SecurityData sec = new SecurityData();
    private static MarketTick tick1 = new MarketTick();
    private static MarketTick tick2 = new MarketTick();
    private static Tick bid = new Tick();
    private static Tick ask = new Tick();

    public static void main(String args[]) {
        String os = System.getProperty("os.name");
        System.out.println("OS Name = "+os);

        int n = Integer.parseInt(System.getProperty("TN", "0"));

        long size = LockFreeRingBuffer.getMemorySizeFor(MarketDataEvent.SIZE, RING_SIZE);

        NamedMemoryInf memSource = NativeResource.getNamedSharedMemory("MarketData", cleanupRequired);
        long memEv = memSource.create("Event", size, 0);
        LockFreeRingBuffer<MarketDataEvent> rb = new LockFreeRingBuffer<>(memEv, MarketDataEvent.SIZE, RING_SIZE);

        long cacheSize = SharedCache.getMemorySizeFor(MarketProduct.SIZE, MAX_PRODUCTS); // 5000 products
        long memMk = memSource.create("TickCache", cacheSize, 0);
        SharedCache<MarketProduct> tickCache = new SharedCache<>(memMk, MarketProduct.SIZE, MAX_PRODUCTS);
        initialLoadingProducts(tickCache, mp, sec);
        FeedSubscriptionMonitor mon = new FeedSubscriptionMonitor(tickCache);
        mon.start();
        if(n > 0)
            testMultiThreaded(rb, tickCache, n);
        produce(rb, tickCache, loop, publishEvent, requestedEventType, waitLoop, notifyConsumer);
    }

    private static void produce(LockFreeRingBuffer<MarketDataEvent> rb,
                                SharedCache<MarketProduct> tickCache,
                                long loop, boolean publishEvent, int requestedEventType,
                                boolean waitLoop, boolean notifyConsumer) {

        NotifierWaitStrategy notifier = null;
        if(notifyConsumer)
            notifier = new NotifierWaitStrategy("NotificationForConsumerOfLFQ");
        CPUAffinity cpu = NativeResource.getCPUAffinity();
        cpu.setThreadHandle();
        cpu.setAffinity(2);

//        try {
//            System.out.print("Press Enter key...");
//            while(System.in.available()==0);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        long max_loop;
        long queueFullCnt;
        long seq = rb.getSequenceNumber();
        boolean startedPublish = false;
        while (true) {

            max_loop = loop;
            queueFullCnt = 0;
            long start = System.nanoTime();
            int pid;
            long ver;
            //System.out.println("max_loop="+max_loop);
            for(long i = 0L; i < max_loop; ++i) {
                int ev;
                switch(requestedEventType) {
                    case -1: ev = (int) (seq % 6L); break;
                    default: ev = requestedEventType;
                }

                //tickCache.seek(mp, seq % tickCache.capacity());
                pid = (int)(i & 0x0FFFL);
                tickCache.seek(mp, pid );
                mp.getSecurityData(sec);
                if(sec.getStatus() < sec.SUBSCRIBED)
                    continue;

                mp.acquire();
                switch(ev) {
                    case 0:
                        mp.getOpenTick(tick1);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        break;
                    case 1:
                        mp.getCloseTick(tick1);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        break;
                    case 2:
                        mp.getLastTradeTick(tick1);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        break;
                    case 3:
                        mp.getNBBO(tick1, tick2);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        break;
                    case 4:
                        mp.getBBOMarket(0, bid, ask);
                        bid.putPrice(seq / 10000.0);
                        bid.putQuantity(100);
                        bid.putTimeStamp(seq);
                        break;
                    case 5:
                        mp.getOpenTick(tick1);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        mp.getCloseTick(tick1);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        mp.getLastTradeTick(tick1);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        mp.getNBBO(tick1, tick2);
                        tick1.putPrice(seq / 10000.0);
                        tick1.putQuantity(100);
                        tick1.putTimeStamp(seq);
                        tick1.putMarketCode(5);
                        mp.getBBOMarket(0, bid, ask);
                        bid.putPrice(seq / 10000.0);
                        bid.putQuantity(100);
                        bid.putTimeStamp(seq);
                        break;
                    case 63: // no operation
                        break;
                    default:
                        System.out.println("Invalid event generated: "+ev+" by seq="+seq);
                }
                //Utility.doDelay(100);
                ver = sec.increaseVersion();
                mp.commit();
                //System.out.println("publishEvent="+publishEvent+", readerAvailable="+(!rb.emptyReader()));
                if(publishEvent && !rb.emptyReader()) {
                    if(!startedPublish) {
                        //System.out.println("Waiting for reader(s)...");
                        //System.out.println("Reader(s) detected, start to produce events, seq="+seq);
                        rb.reserve(mde);
                        mde.setSequence(++seq);
                        mde.setEvent(mde.makeEvent(0, 60, 0)); // start signal for measurement
                        rb.commit();
                        startedPublish = true;
                    }
                    else {
                        boolean available;
                        do {
                            available = rb.reserve(mde, false);
                            if(!available) {
                                // queue full
                                ++queueFullCnt;
                            }
                            else {
                                mde.setSequence(++seq);
                                if(pid >= MAX_PRODUCTS) {
                                    System.out.println("productId is incorrectly generated="+pid);
                                    return;
                                }
                                mde.setEvent(mde.makeEvent(pid, ev, ver));
                                rb.commit();
                                if(notifier != null)
                                    notifier.broadcast();
                            }
                        } while(waitLoop && !available && !rb.emptyReader() && Utility.busyDelay((int) queueFullCnt, (int) queueFullCnt / 10000));

                        //else {
                        //    rb.reserve(mde);
                        //    mde.setSequence(++seq);
                        //    mde.setEvent(mde.makeEvent(pid, ev, 0));
                        //    rb.commit();

                    }
                }
            }
            double diff = (System.nanoTime() - start)/(1.0*max_loop);
            System.out.println("Finished: seq="+seq+" nsec-per-slot="+diff+(publishEvent? ", queue_full_cnt="+queueFullCnt:""));

            if(publishEvent && !rb.emptyReader()) {
                //--- end signal for measurement
                rb.reserve(mde);
                mde.setSequence(++seq);
                mde.setEvent(mde.makeEvent(0, 61, 0));
                rb.commit();
                startedPublish = false;
                queueFullCnt = 0;
            }
        }
    }

    //--------------------------
    private static void testMultiThreaded(LockFreeRingBuffer<MarketDataEvent> rb,
                                          SharedCache<MarketProduct> tickCache,
                                          int n) {
        int[] cpumap = new int[] {4, 6, 8, 10, 3, 5, 7, 9, 11};
        List<Thread> consumers = new ArrayList<>();
        for(int i = 0; i < n; ++i) {
            MarketDataRealTimeEventProcessor mdeProc = new MarketDataRealTimeEventProcessor(i+1, tickCache);
            ReaderInf r = rb.genReader();
            ConsumerProxy<MarketDataEvent,MarketDataRealTimeEventProcessor> c = new ConsumerProxy<>(r, mdeProc);
            Thread t = new Thread(c);
            consumers.add(t);
            c.setAffinity(cpumap[r.getId()]);
        }
        for(Thread t : consumers)
            t.start();

//        try {
//            for( Thread t :  consumers)
//                t.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    private static void initialLoadingProducts(SharedCache<MarketProduct> cache, MarketProduct mp, SecurityData sec) {
        long size = cache.capacity();
        for(long i = 0; i < size; ++i) {
            cache.seek(mp, i);
            mp.getSecurityData(sec).activate();
        }
    }
}
