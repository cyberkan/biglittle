package biglittle.test;

import biglittle.systemlib.Utility;

public class TimeTest {
    private static int max_loop = 10000000;

    public static void main(String[] args) throws Exception {
        System.out.println("initialize utility...");
        long t1 = System.nanoTime();
        Utility.microTime();
        System.out.println("Milliseconds to sync="+(System.nanoTime()-t1)/1000000+"msec");
        //System.out.println("Warmup...");
        //measureMilliTime(false);
        //measureNanoTime(false);
        //measureMilliTime(true);
        //measureNanoTime(true);
        //measureMicroTime(true);
        int max_loop = 200;
        long[] u1 = new long[max_loop];
        long[] u2 = new long[max_loop];
        // delay to catch the edge
        System.out.println("sync to the edge");
        while(Utility.microTime() % 1000L < 990);
        for(int i = 0; i < max_loop; ++i) {
            u1[i] = System.currentTimeMillis();
            u2[i] = Utility.microTime();
        }
        for(int i = 0; i < max_loop; ++i) {
            System.out.println(i+" "+u1[i]*1000L);
            System.out.println(i+" "+u2[i]);
        }
        for(int i = 0; i < 1000; ++i) {
            while(System.currentTimeMillis() % 1000L > 0L);
            long milli = System.currentTimeMillis();
            long micro = Utility.microTime();
            System.out.printf("milli-micro: %d-%d : %06d-%06d : %d\n",milli/1000L,micro/1000000L,(milli%1000L)*1000L,+micro%1000000L, micro-milli*1000);
            Utility.usleep(990000);
        }
    }

    private static void measureNanoTime(boolean print) {
        long start = System.nanoTime();
        long test = 0;
        for(int i = 0; i < max_loop; ++i) {
            test += System.nanoTime();
        }
        double diff = System.nanoTime() - start;
        if(print)
            System.out.println("nanoTime():          "+(diff/max_loop)+" per-call "+test);
    }
    private static void measureMicroTime(boolean print) {
        long start = System.nanoTime();
        long test = 0;
        for(int i = 0; i < max_loop; ++i) {
            test += Utility.microTime();
        }
        double diff = System.nanoTime() - start;
        if(print)
            System.out.println("microTime():         "+(diff/max_loop)+" per-call "+test);
    }
    private static void measureMilliTime(boolean print) {
        long start = System.currentTimeMillis();
        long test = 0;
        for(int i = 0; i < max_loop; ++i) {
            test += System.currentTimeMillis();
        }
        double diff = System.currentTimeMillis() - start;
        if(print)
            System.out.println("currentTimeMillis(): "+(diff*1000000/max_loop)+" per-call "+test);
    }
}
