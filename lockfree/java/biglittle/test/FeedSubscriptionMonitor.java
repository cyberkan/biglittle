package biglittle.test;

import biglittle.ipc.NotifierInf;
import biglittle.ipc.primitive.SharedCache;
import biglittle.systemlib.NativeResource;
import biglittle.test.marketdata.MarketProduct;
import biglittle.test.marketdata.SecurityData;

public class FeedSubscriptionMonitor extends Thread {

    SharedCache<MarketProduct> productCache;
    MarketProduct mp = new MarketProduct();
    SecurityData sec = new SecurityData();

    FeedSubscriptionMonitor(SharedCache<MarketProduct> productCache) {
        this.productCache = productCache;
    }

    public void run() {
        NotifierInf noti = NativeResource.getNamedNotifier("SubscriptionNotification");
        long numRecs = productCache.capacity();
        while(true) {
            System.out.println("Waiting for subscriptions to products");
            noti.waitFor();
            for(long i = 0; i < numRecs; ++i) {
                productCache.seek(mp, i);
                if(mp.getSecurityData(sec).getStatus() == sec.INTERESTED) {
                    // make subscription to Feeder
                    long newStatus = sec.subscribing();
                    if(newStatus == sec.SUBSCRIBING) {
                        //System.out.println("Subscribing Product "+i);
                        // send subscription to the feed provider
                        // wait for the response
                        sec.subscribed();
                    }
                }
                if(sec.getStatus() == sec.UNSUBSCRIBING) {
                    // send unsubscription
                    sec.unsubscribed();
                }
            }
        }
    }
}
