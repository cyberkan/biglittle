import biglittle.systemlib.VirtualKMEvent;
import biglittle.systemlib.windows.User32Lib;
import biglittle.systemlib.windows.Win_Input_C;
import biglittle.systemlib.windows.Win_KeyboardInput_C;
import biglittle.systemlib.windows.Win_MouseInput_C;
import com.sun.jna.platform.win32.Kernel32;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class VirtualKMClient extends Thread {
    private static User32Lib user32 = User32Lib.INSTANCE;
    Kernel32 kernel32 = Kernel32.INSTANCE;
    private byte[] buf = new byte[0x0FFFF];
    private DatagramPacket packet = new DatagramPacket(buf, buf.length);
    private VirtualKMEvent kmevent = new VirtualKMEvent(packet.getData());

    public static void main(String[] args) throws Exception {
        //if(args.length < 1) {
        //    System.err.println("Usage : program <ip>");
        //    return;
        //}
        Thread t = new VirtualKMClient();
        Thread s = new KMSubscriber("226.67.11.9", 6712);
        t.start();
        s.start();
        t.join();
    }
    public void run() {
        Win_Input_C.ByReference input = new Win_Input_C.ByReference();

        try {
            DatagramSocket socket = new DatagramSocket(6711);
            while(true) {
                socket.receive(packet);
                //kmevent.setBuffer(packet.getData());
                int type = kmevent.getType();
                int code = kmevent.getCode();
                int value = kmevent.getValue();
                int ret;
                switch(type) {
                    case VirtualKMEvent.EV_SYN: // 0
                        break;
                    case VirtualKMEvent.EV_REL: // 2
                        translateMouseMoveEvent(code, value, input);
                        ret = user32.SendInput(1, input, input.size());
                        System.out.println("mouse move-ret="+ret);
                        break;
                    case VirtualKMEvent.EV_KEY: // 1
                        switch(code) {
                            // mouse buttons
                            case VirtualKMEvent.BTN_LEFT:
                            case VirtualKMEvent.BTN_RIGHT:
                            case VirtualKMEvent.BTN_MIDDLE:
                                translateMouseButtonEvent(code, value, input);
                                ret = user32.SendInput(1, input, input.size());
                                System.out.printf("mouse button-ret=%d (%d),dwFlags=%04x\n",
                                                  ret, kernel32.GetLastError(), input.in.mi.dwFlags);
                                break;
                            default:
                                transalteKeyboardEvent(code, value, input);
                                ret = user32.SendInput(1, input, input.size());
                                break;
                        }
                        break;
                    case VirtualKMEvent.EV_MSC: // 4
                        break;
                }
                //byte[] buf = packet.getData();
                //System.out.printf("sender=%s, size=%d:type=%d,code=%04x:0x%02x%02x \n",
                //        packet.getAddress().toString(),
                //        packet.getLength(),
                //        kmevent.getType(),
                //        kmevent.getCode(),
                //        buf[0], buf[1]);
            }
        }
        catch (SocketException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void translateMouseButtonEvent(int code, int value, Win_Input_C.ByReference input) {
        input.type = Win_Input_C.INPUT_MOUSE;
        input.in.setType("mi");
        input.in.mi.mouseData = 0;
        switch(code) {
            // mouse buttons
            case VirtualKMEvent.BTN_LEFT:
                if(value == 1)
                    input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_LEFTDOWN;
                else
                    input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_LEFTUP;
                break;
            case VirtualKMEvent.BTN_RIGHT:
                if(value == 1)
                    input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_RIGHTDOWN;
                else
                    input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_RIGHTUP;
                break;
            case VirtualKMEvent.BTN_MIDDLE:
                if(value == 1)
                    input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_MIDDLEDOWN;
                else
                    input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_MIDDLEUP;
                break;
        }
    }
    private void translateMouseMoveEvent(int code, int value, Win_Input_C.ByReference input) {
        input.type = Win_Input_C.INPUT_MOUSE;
        input.in.setType("mi");
        input.in.mi.mouseData = 0;
        switch (code) {
            case VirtualKMEvent.REL_X:
                input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_MOVE;
                input.in.mi.dx = value;
                input.in.mi.dy = 0;
                break;
            case VirtualKMEvent.REL_Y:
                input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_MOVE;
                input.in.mi.dx = 0;
                input.in.mi.dy = value;
                break;
            case VirtualKMEvent.REL_HWHEEL:
                input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_HWHEEL;
                input.in.mi.mouseData = Win_Input_C.MouseInput.WHEEL_DELTA*value;
                break;
            case VirtualKMEvent.REL_WHEEL:
                input.in.mi.dwFlags = Win_Input_C.MouseInput.MOUSEEVENTF_WHEEL;
                input.in.mi.mouseData = Win_Input_C.MouseInput.WHEEL_DELTA*value;
                break;
        }
    }
    private void transalteKeyboardEvent(int code, int value, Win_Input_C.ByReference input) {
        input.type = Win_Input_C.INPUT_KEYBOARD;
        input.in.setType("ki");
        input.in.ki.dwFlags = Win_Input_C.KeyInput.KEYEVENTF_SCANCODE;
        input.in.ki.wScan = (short)code;
        switch(value) {
            case 0: // up
                input.in.ki.dwFlags |= Win_Input_C.KeyInput.KEYEVENTF_KEYUP;
            case 1: // pressed
                break;
            case 2: // repeated
        }
    }
}
