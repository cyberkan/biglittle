import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

/**
 * Created by Dan on 1/2/2016.
 */
public class KMSubscriber extends Thread {
    private static final long interval = Long.parseLong(System.getProperty("SubscriptionInterval", "500"));
    private String ip = System.getProperty("LocalInetAddress");
    private MulticastSocket socket = null;
    private DatagramPacket packet = null;
    private InetAddress address;
    private NetworkInterface iface;

    public KMSubscriber(String addr, int port) {
        try {
            socket = new MulticastSocket();
            socket.setTimeToLive(1);
            //ip = InetAddress.getLocalHost().getHostAddress();
            //
            //String ip;
            if(ip != null && !ip.isEmpty()) {
                iface = NetworkInterface.getByInetAddress(Inet4Address.getByName(ip));
            }
            else try {
                boolean selected = false;
                Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces.hasMoreElements()) {
                    NetworkInterface iface = interfaces.nextElement();
                    // filters out 127.0.0.1 and inactive interfaces
                    if (iface.isLoopback() || !iface.isUp())
                        continue;
                    System.out.println("Interface="+iface+", isVirtual="+(iface.isVirtual()?"yes":"no"));

                    Enumeration<InetAddress> addresses = iface.getInetAddresses();
                    while(addresses.hasMoreElements()) {
                        InetAddress inet_addr = addresses.nextElement();
                        if(inet_addr instanceof Inet4Address) {
                            ip = inet_addr.getHostAddress();
                            this.iface = iface;
                            System.out.println(iface.getDisplayName() + ": " + ip);
                            selected = true;
                            break;
                        }
                    }
                    if(selected)
                        break;
                }
            } catch (SocketException e) {
                throw new RuntimeException(e);
            }
            socket.setNetworkInterface(this.iface);
            System.out.println("interface="+iface+", ip="+ip);
            //
            byte[] b = ip.getBytes(StandardCharsets.UTF_8);
            byte[] buf=new byte[b.length+1];
            System.arraycopy(b, 0, buf, 0, b.length);
            address = InetAddress.getByName(addr);
            packet = new DatagramPacket(buf, buf.length, address, port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void run() {

        while(true) {
            try {
                //System.out.println("sending subscription: ip="+ip);
                socket.send(packet);
                sleep(interval);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
