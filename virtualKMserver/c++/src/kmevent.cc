//#include <ThreadCpuAffinity.h>

#include <linux/input.h>

#include <string>
#include <iostream>
#include <algorithm>
#include <unordered_map>

#include <DataProcessor.h>
#include <StreamSocketListener.h>
#include <DatagramSocketListener.h>
#include <Multiplexer.h>
#include <EventDeviceListener.h>

//---------------------------------------------------------
class KMEventSender : public DataProcessor
{
    DatagramClientSocketListener & sender;

    union KeySequence {
        uint32_t flag;
        uint8_t sequence[4];
    };
    KeySequence sw;

    public:
    KMEventSender(DatagramClientSocketListener &sender_) : sender(sender_) {
        sw.flag = 0xFFFFFFFF;
    }

    virtual void operator()(char * buf, size_t buf_len, size_t len) override {
        if(buf_len < len)
            return;
        sender.send(buf, len);
        //------
        struct input_event *ev = (struct input_event *) buf;
        if(ev->type == EV_KEY) {
            switch(ev->code) {
                case KEY_LEFTCTRL:
                    if(ev->value == 1)
                        sw.sequence[0] = 0xFF;
                    else if(ev->value == 0)
                        sw.sequence[0] = 0;
                    break;
                case KEY_LEFTSHIFT:
                    if(ev->value == 1)
                        sw.sequence[1] = 0xFF;
                    else if(ev->value == 0)
                        sw.sequence[1] = 0;
                    break;
                case KEY_LEFTALT:
                    if(ev->value == 1)
                        sw.sequence[2] = 0xFF;
                    else if(ev->value == 0)
                        sw.sequence[2] = 0;
                    break;
                case KEY_F1:
                    if(ev->value == 1)
                        sw.sequence[3] = 0xFF;
                    else if(ev->value == 0)
                        sw.sequence[3] = 0;
                    break;
            }
        }
        if(sw.flag == 0) {
            printf("switch to the next connection\n");
            sw.flag = 0xFFFFFFFF;
        }
    }
};

class SubscriptionManager : public DataProcessor
{
    struct SubscriptionRecord
    {
        time_t time;
        std::string address;
    };

    DatagramClientSocketListener & sender;
    std::unordered_map<std::string, SubscriptionRecord> subscribers;
    std::string current;

    public:
    SubscriptionManager(DatagramClientSocketListener &sender_) : sender(sender_) {
    }

    virtual void operator()(char * buf, size_t buf_len, size_t len) override {
        if(buf_len < len)
            return;
        SubscriptionRecord rec;
        rec.time = time(0);
        rec.address = buf;
        auto iter = subscribers.insert({rec.address,rec});
        if(!iter.second) {
            iter.first->second.time = rec.time;
            //printf("updating subscriber(%s) timestamp : %d\n", rec.address.c_str(), rec.time);
        }
        else {
            printf("new subscriber : %s\n", rec.address.c_str());
            if(subscribers.size() == 1) {
                sender.setServerAddress(buf);
                current = buf;
            }
        }
    }

    void cleanUp() {
        std::vector<std::string> expired_list;
        for(auto& rec : subscribers) {
            time_t now = time(0);
            if((now - rec.second.time) > 10) {
                printf("unsubscribe ip=%s:%d, current=%s:%ld\n",
                        rec.second.address.c_str(), rec.second.address.size(),
                        current.c_str(), current.size());
                if(current == rec.second.address) {
                    current.clear();
                    sender.setServerAddress(current);
                }
                expired_list.push_back(rec.second.address);
            }
        }
        for(std::string key : expired_list)
            subscribers.erase(key);
        if(current.empty())
            switchToNext();
    }

    void switchToNext() {
        if(subscribers.size() == 0)
            return;
        if(current.empty()) {
            current = subscribers.begin()->second.address;
        }
        else {
            auto iter = subscribers.find(current);
            if(iter == subscribers.end() || ++iter == subscribers.end())
                current = subscribers.begin()->second.address;
            else
                current = iter->second.address;
        }
        sender.setServerAddress(current);
    }

    private:

};

int main (int argc, char *argv[])
{
    if (argc < 2 ){
        printf("Please specify (on the command line) the path to the dev event interface for mouse and keyboard\n");
        exit (0);
    }

    try {
        Multiplexer selector;
        MulticastServerSocketListener<1024> subs("192.168.1.191", "226.67.11.9", 6712);
        DatagramClientSocketListener senderSocket(6711);
        EventDeviceListener mouse(argv[1]), keyboard(argv[2]);
        KMEventSender sender(senderSocket);
        SubscriptionManager subsManager(senderSocket);

        mouse.setDataProcessor(&sender);
        keyboard.setDataProcessor(&sender);
        subs.setDataProcessor(&subsManager);
        selector.add(mouse);
        selector.add(keyboard);
        selector.add(subs);
        while (1) {
            int n = selector.poll(1);
            if (n == 0)
                subsManager.cleanUp();
            if (n < 0) {
                std::cerr << "poll() error, exiting\n";
                break;
            }
        }
    }
    catch(char const* e) {
        std::cerr << "Exception:" << e << std::endl;
    }
    return 0;
}